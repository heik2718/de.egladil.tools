/**
 *
 */
package de.egladil.tools.restclient;

import static org.junit.Assert.assertTrue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Test;

/**
 * @author winkelv
 *
 */
public class SSLRestClientTest {

	private static final String BASE_URI = "https://mathe-jung-alt.de/mkvapi";

	@Test
	public void testPing() {

		// Arrange
		Client client = RestSSLUtils.createSSLCient();

		// Act
		WebTarget target = client.target(BASE_URI + "/session/kontext");
		Response response = target.request().get();
		String responsePayload = response.readEntity(String.class);

		System.out.println(responsePayload);

		assertTrue(responsePayload.contains("\"environment\":\"PROD\""));

	}

//	@Test
	public void testPingBehindProxy() {

		// Arrange
//		System.getProperties().put("https.proxyHost", "xxxx");
//		System.getProperties().put("https.proxyPort", "80");
//		System.getProperties().put("https.proxyUser", "xxxxxx");
//		System.getProperties().put("https.proxyPassword", "xxxxxxx");
//		System.getProperties().put("https.proxySet", "true");

		Client client = RestSSLUtils.createSSLCient();

		// Act
		WebTarget target = client.target(BASE_URI + "/session/kontext");
		Response response = target.request().get();
		String responsePayload = response.readEntity(String.class);

		System.out.println(responsePayload);

	}

}
