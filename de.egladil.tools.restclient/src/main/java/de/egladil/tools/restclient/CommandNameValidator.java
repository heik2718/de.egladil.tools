//=====================================================
// Projekt: de.egladil.tools.restclient
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.restclient;



import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;


public class CommandNameValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		if (!SupportedCommand.getCLIValues().contains(value)) {
			throw new ParameterException(
				"unsupported value for option -c! expect one of " + SupportedCommand.printSupportedCLIValues());
		}
	}
}
