//=====================================================
// Projekt: de.egladil.tools.restclient
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.restclient;

import java.util.List;

public final class PrettyStringUtils {

	public static String listToString(List<String> strings) {
		if ((strings == null) || (strings.isEmpty())) {
			return "";
		}
		if (strings.size() < 2) {
			return strings.get(0);
		}
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < strings.size() - 1; i++) {
			sb.append(strings.get(i));
			sb.append(", ");
		}
		sb.append(strings.get(strings.size() - 1));
		return sb.toString();
	}
}
