// =====================================================
// Project: de.egladil.tools.restclient
// (c) Heike Winkelvoß
// =====================================================
package de.egladil.tools.restclient;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

/**
 * RestSSLUtils<br>
 * <br>
 * Vorbereitung: das Zertifikat muss in den lokalen Java-Keystore importiert
 * werden.
 * <ul>
 * <li><strong>1.</strong> Über den Browser das Zertifikat als Datei Speichern
 * (am besten Base64-encoded). Dateiname sei zertifikat.crt</li>
 * <li><strong>2.</strong> CommandLine im Verzeichnis mit dem Zertifikat
 * öffnen</li>
 * <li><strong>3.</strong> keytool -import -alias bla -file zertifikat.cer
 * -keystore %JAVA_HOME%\jre\lib\security\cacerts</li>
 * <li><strong>4.</strong> Man wird nach dem Keystore-Passwort gefragt. Das ist
 * 'changeit'</li>
 * <li><strong>5</strong> Man wird gefragt, ob man dem Zertifikat vertraut:
 * Ja/Yes</li>
 * </ul>
 */
public final class RestSSLUtils {

	/**
	 * Erzeugt eine Instanz von RestSSLUtils
	 */
	private RestSSLUtils() {
	}

	/**
	 * Erzeugt einen SSL-fähigen RestClient
	 *
	 * @return Client
	 */
	public static Client createSSLCient() {

		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			String trustStorePasswd = "changeit";

			String javahome = System.getenv("JAVA_HOME");

			String truststoreFile = null;

			if (javahome != null) {
				truststoreFile = javahome + "/jre/lib/security/cacerts";
			} else {

				String os = System.getProperty("os.name");

				if ("linux".equalsIgnoreCase(os)) {
					truststoreFile = "/etc/ssl/certs/java/cacerts";
				} else {
					truststoreFile = "d:/projekte/_SOFTWARE_/JAVA/java-1.8.0-openjdk-1.8.0.222-4.b10.redhat.windows.x86_64/jre/lib/security/cacerts";
				}

			}

			try (FileInputStream fis = new FileInputStream(truststoreFile)) {

				trustStore.load(fis, trustStorePasswd.toCharArray());
				// Default trust manager is PKIX (No SunX509)
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				tmf.init(trustStore);
				SSLContext sslContext = SSLContext.getInstance("TLS");
				sslContext.init(null, tmf.getTrustManagers(), null);
				return ClientBuilder.newBuilder().sslContext(sslContext).build();
			} catch (Exception e) {
				throw new RuntimeException("Erzeugung eines SSL-fähigen Clients nicht möglich wegen " + e.getMessage(),
						e);
			}
		} catch (KeyStoreException e) {
			throw new RuntimeException("Erzeugung eines SSL-fähigen Clients nicht möglich wegen " + e.getMessage(), e);
		}
	}
}
