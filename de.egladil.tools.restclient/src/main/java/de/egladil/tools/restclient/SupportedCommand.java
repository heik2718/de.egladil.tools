//=====================================================
// Projekt: de.egladil.tools.restclient
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.restclient;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.ParameterException;

public enum SupportedCommand {
	UPLOAD("upload"),
	DOWNLOAD("download"),
	POST_REST_REQUEST("post"),
	GET_REST_REQUEST("get");

	private final String cliValue;

	private SupportedCommand(String cliValue) {
		this.cliValue = cliValue;
	}

	public String getCliValue() {
		return this.cliValue;
	}

	public static SupportedCommand valueOfName(String name) {
		for (SupportedCommand supportedCommand : SupportedCommand.values()) {
			if (supportedCommand.getCliValue().equals(name)) {
				return supportedCommand;
			}
		}
		throw new ParameterException("unsupported value for option -c! expect one of " + printSupportedCLIValues());
	}

	public static String printSupportedCLIValues() {
		return PrettyStringUtils.listToString(getCLIValues());
	}

	public static List<String> getCLIValues() {
		List<String> alle = new ArrayList<String>();
		for (SupportedCommand supportedCommand : values()) {
			alle.add(supportedCommand.getCliValue());
		}
		return alle;
	}
}
