//=====================================================
// Projekt: de.egladil.tools.restclient
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.restclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class EgladilRestClient {

	private static final Logger LOG = LoggerFactory.getLogger(EgladilRestClient.class);

	private String HEADER_VALUE_UPLOAD_ACCEPT_PREFIX = "text/plain; charset=UTF-8; version=";

	private String HEADER_VALUE_DOWNLOAD_ACCEPT_PREFIX = "application/gzip; charset=UTF-8; version=";

	private String HEADER_KEY_ACCEPT = "Accept";

	@Parameter(names = {
		"-c" }, description = "commandname: [upload|download|post|get] (upload gzip or download zip or post rest rquest or get rest request)", required = true, validateWith = CommandNameValidator.class)
	private String cmdName;

	@Parameter(names = { "-u" }, description = "REST url - mandatory", required = true)
	private String url;

	@Parameter(names = { "-f" }, description = "Pfad zum Payload")
	private String pathToFile;

	@Parameter(names = { "-v" }, description = "REST-API-Version 4.4, 4.8, ...")
	private String apiVersion;

	@Parameter(names = { "-k" }, description = "Kennung der Betriebsstaette", required = true)
	private String kennung;

	@Parameter(names = { "-p" }, description = "password der Betriebsstaette", required = true, password = true)
	private String password;

	public static void main(String[] args) {
		EgladilRestClient application = null;
		try {
			application = new EgladilRestClient();
			new JCommander(application, args);
			LOG.info(application.toString());
			application.start();
		} catch (ParameterException e) {
			System.err.println(e.getMessage());
			if (application != null) {
				application.printUsage();
			}
			System.exit(1);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			System.exit(1);
		}
	}

	private void start() throws IOException {
		SupportedCommand command = SupportedCommand.valueOfName(cmdName.toLowerCase());
		File file = null;
		switch (command) {
		case UPLOAD:
			file = new File(pathToFile);
			this.upload(file, kennung, password, apiVersion);
			break;
		case DOWNLOAD:
			this.download(kennung, password, apiVersion);
			break;
		case GET_REST_REQUEST:
			this.get();
			break;
		default:
			LOG.info("Funktion {} noch nicht implementiert", command.name());
			break;
		}
	}

	private void printUsage() {
		StringBuffer sb = new StringBuffer();
		sb.append("Usage: <main class> [options]\n");
		sb.append("   Options:\n");
		sb.append("     * -c\n");
		sb.append(
			"          commandname: [upload|download|post|get] (upload gzip or download gzip or post rest reqest)\n");
		sb.append("     * -u\n");
		sb.append("          REST url\n");
		sb.append("     -f\n");
		sb.append("          Pfad zum Payload - also XML oder gzip\n");
		sb.append("     -v\n");
		sb.append("          API-Version (4.0, 4.8,...\n");
		sb.append("     * -k\n");
		sb.append("          Kennung der Betriebsstätte\n");
		sb.append("     * -p\n");
		sb.append("          Passwort der Betriebsstätte\n");
		sb.append("\nBeispiel:\n\n");
		sb.append(
			"upload Batch 2.0 - 4.0: <main class> -c upload -u http://oadevsap1.itshessen.hessen.de:50000/oasisbatchws/rest/oasis/anlegen/auftrag/batch/4.0 -f <Pfad zum gzip> -k Kennung der Betriebsstaette -p Passwort der Betriebsstaette\n");
		sb.append("\n");
		sb.append(
			"upload Batch 2.0 - 4.8: <main class> -c upload -u http://oadevsap1.itshessen.hessen.de:50000/oasisbatchws/batch -f <Pfad zum gzip> -v 4.8 -k Kennung der Betriebsstaette -p Passwort der Betriebsstaette\n");
		sb.append("\n");
		sb.append(
			"download Batch 2.0 - 4.0: <main class> -c download -u http://oadevsap1.itshessen.hessen.de:50000/oasisbatchws/rest/oasis/abfragen/auftrag/batch/4.0/{batchID} -k Kennung der Betriebsstaette -p Passwort der Betriebsstaette\n");
		sb.append("\n");
		sb.append(
			"download Batch 2.0 - 4.8: <main class> -c download -u http://oadevsap1.itshessen.hessen.de:50000/oasisbatchws/batch/{batchID} -v 4.8 -k Kennung der Betriebsstaette -p Passwort der Betriebsstaette\n");
		sb.append("\n");

		System.out.println(sb.toString());
	}

	private void upload(File input, String kennung, String password, final String apiVersion)
		throws HttpException, IOException {
		// Get target URL
		PostMethod post = new PostMethod(url);

		post.setRequestEntity(new InputStreamRequestEntity(new FileInputStream(input), input.length()));

		post.setRequestHeader("Content-type", "application/gzip; charset=UTF-8");
		if (apiVersion != null) {
			post.setRequestHeader(HEADER_KEY_ACCEPT, HEADER_VALUE_UPLOAD_ACCEPT_PREFIX + apiVersion);
			post.setRequestHeader("Kennung", kennung);
			post.setRequestHeader("Passwort", password);
		} else {
			post.setRequestHeader("OASIS_KENNUNG", kennung);
			post.setRequestHeader("OASIS_PWD", password);
		}
		// Get HTTP client
		HttpClient httpclient = new HttpClient();

		// Execute request
		try {

			int result = httpclient.executeMethod(post);

			// Display status code
			LOG.info("Response status code: {}", result);

			// Display response
			LOG.info("Response body:");
			LOG.info(post.getResponseBodyAsString());

		} finally {
			// Release current connection to the connection pool
			// once you are done
			post.releaseConnection();
		}
	}

	private void download(String kennung, String password, final String apiVersion) throws HttpException, IOException {
		// Prepare HTTP post
		HttpMethodBase method = null;

		// = new PostMethod(url);

		if ("4.8".equals(apiVersion)) {
			method = new GetMethod(url);
		} else {
			method = new PostMethod(url);
		}

		method.setRequestHeader("Content-type", "text/plain; charset=UTF-8");
		method.setRequestHeader("OASIS_KENNUNG", kennung);
		method.setRequestHeader("OASIS_PWD", password);
		if (apiVersion != null) {
			method.setRequestHeader(HEADER_KEY_ACCEPT, HEADER_VALUE_DOWNLOAD_ACCEPT_PREFIX + apiVersion);
		}

		// Get HTTP client
		HttpClient httpclient = new HttpClient();

		// Execute request
		boolean success = false;
		try {

			int result = httpclient.executeMethod(method);
			String responseBodyAsString = method.getResponseBodyAsString();
			if (result == 200 && responseBodyAsString != null && !responseBodyAsString.isEmpty()) {
				success = true;
			}

			// Display status code
			LOG.info("Response status code: {}", result);
			// LOG.info(responseBodyAsString);
		} finally {
			// Release current connection to the connection pool
			method.releaseConnection();
		}

		// write response to File
		if (success) {
			InputStream initialStream = null;
			try {
				initialStream = method.getResponseBodyAsStream();
				this.writeFile(initialStream);
			} finally {
				IOUtils.closeQuietly(initialStream);
			}
		}
	}

	private void writeFile(InputStream initialStream) throws HttpException, IOException {
		OutputStream outStream = null;
		try {
			String workingDir = System.getProperty("user.dir");
			String pathOutputDir = workingDir + File.separator + "download";
			File outputDir = new File(pathOutputDir);
			if (!outputDir.isDirectory()) {
				outputDir.mkdir();
			}

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
			String targetFileName = sdf.format(new Date()) + "_responseFile.gz";

			File targetFile = new File(pathOutputDir + File.separator + targetFileName);
			outStream = new FileOutputStream(targetFile);

			IOUtils.copy(initialStream, outStream);

			byte[] buffer = new byte[8 * 1024];
			int bytesRead;
			while ((bytesRead = initialStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			outStream.flush();
			LOG.info("Datei heruntergeladen: {}", targetFile.getAbsolutePath());
		} finally {
			IOUtils.closeQuietly(outStream);
		}
	}

	private void get() {
		// Get target URL
		GetMethod get = new GetMethod(url);

		get.setRequestHeader("Content-type", "text/plain; charset=UTF-8");
		// Get HTTP client
		HttpClient httpclient = new HttpClient();

		try {
			int httpResponseCode = httpclient.executeMethod(get);
			LOG.info("Response status code: {}", httpResponseCode);
			LOG.info("Response Body \n {}", get.getResponseBodyAsString());
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * @param in
	 * @return
	 * @throws IOException
	 */
	private InputStream decorateWithLogging(InputStream in) throws IOException {
		if (LOG.isDebugEnabled() && in != null) {
			String xml = IOUtils.toString(in, "UTF-8");
			LOG.debug("Server-Antwort:\n" + xml);
			InputStream result = IOUtils.toInputStream(xml, "UTF-8");
			return result;
		} else {
			return in;
		}
	}
}