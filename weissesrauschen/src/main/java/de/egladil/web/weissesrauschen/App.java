package de.egladil.web.weissesrauschen;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) {

		UrlGenerator urlGenerator = null;
		try {
			urlGenerator = new UrlGenerator();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}

		attachShutDownHook(urlGenerator);
		Entscheider entscheider = new Entscheider();
		HttpClientBuilder clientBuilder = HttpClientBuilder.create();
		// FIXME: user agent konfigurierbar
		clientBuilder.setUserAgent("Mozilla/5.0 (X11; Linux x86_64; rv:38.0) Gecko/20100101 Firefox/38.0 Iceweasel/38.2.1");
		CloseableHttpClient httpclient = clientBuilder.build();
		CloseableHttpResponse response = null;
		try {
			// FIXME: hier kann andere Wörterliste als arg hinterlegt werden.

			while (true) {
				try {
					Thread.sleep(entscheider.pausenlaenge());
				} catch (InterruptedException e) {
				}
				if (entscheider.senden()) {
					String request = urlGenerator.generateUrl(entscheider.anzahlWoerter());
					HttpGet httpGet = new HttpGet(request);
					response = httpclient.execute(httpGet);
					System.out.println(response.getStatusLine());
					HttpEntity httpEntity = response.getEntity();
//					httpEntity.writeTo(System.out);
					EntityUtils.consume(httpEntity);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		} finally {
			IOUtils.closeQuietly(response);
		}
	}

	public static void attachShutDownHook(final UrlGenerator generator) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				generator.printSuchbegriffe();
			}
		});
		System.out.println("Shut Down Hook Attached.");
	}
}
