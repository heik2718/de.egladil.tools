/**
 *
 */
package de.egladil.web.weissesrauschen;

import java.util.Random;

/**
 * @author heike
 *
 */
public class Entscheider {

	private Random random = new Random();

	/**
	 *
	 */
	public Entscheider() {

	}

	public long pausenlaenge() {
//		long result = (random.nextInt(999) + 1) * 1000 + random.nextInt(999);
		long result = (random.nextInt(600) + 1) * 1000 + random.nextInt(101);
		System.out.println("Warte ca. " + (result / 1000 / 60  + 1) + "min ...");
		return result;
	}

	public boolean senden() {
		return random.nextBoolean();
	}

	public int anzahlWoerter() {
		return random.nextInt(2) + 1;
	}
}
