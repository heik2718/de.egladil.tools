/**
 *
 */
package de.egladil.web.weissesrauschen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.IOUtils;

/**
 * @author heike
 *
 */
public class UrlGenerator {

	private static final String GOOGLE = "https://www.google.de/search?q=";

	private List<String> woerter = new ArrayList<String>();

	private List<String> generierteSuchbegriffe = new ArrayList<String>();

	private Random random = new Random();

	private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy h:m:s");

	/**
	 * @throws IOException
	 *
	 */
	public UrlGenerator() throws IOException {
		this("/usr/share/dict/ngerman");
	}

	public UrlGenerator(String pfadZuWortliste) throws IOException {
		List<String> blacklist = readBlacklist();
		readWordlist(pfadZuWortliste, blacklist);
		System.out.println("Anzahl Wörter=" + woerter.size());
		System.out.println("Anzahl ausgeschlossen: " + blacklist.size());
	}

	private void readWordlist(String pfadZuWortliste, List<String> blacklist) throws FileNotFoundException,
		UnsupportedEncodingException, IOException {
		InputStream in = null;
		BufferedReader br = null;
		try {
			in = new FileInputStream(new File(pfadZuWortliste));
			br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				if (!blacklist.contains(line)) {
					woerter.add(line);
				}
			}

		} finally {
			IOUtils.closeQuietly(br);
			IOUtils.closeQuietly(in);
		}
	}

	private List<String> readBlacklist() throws IOException {
		List<String> result = new ArrayList<String>();
		InputStream in = null;
		BufferedReader br = null;
		try {
			in = this.getClass().getResourceAsStream("/blacklist");

			br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				if (!result.contains(line)) {
					result.add(line);
				}
			}
		} finally {
			IOUtils.closeQuietly(br);
			IOUtils.closeQuietly(in);
		}
		return result;
	}

	public String generateUrl() {
		return this.generateUrl(1);
	}

	public String generateUrl(int anzahlSuchbegriffe) {
		int anzahlWoerter = woerter.size();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < anzahlSuchbegriffe - 1; i++) {
			sb.append(woerter.get(random.nextInt(anzahlWoerter)));
			sb.append(" ");
		}
		sb.append(woerter.get(random.nextInt(anzahlWoerter)));
		String suchbegriff = sb.toString();
		System.out.println(sdf.format(new Date()) + " : " + suchbegriff);
		generierteSuchbegriffe.add(suchbegriff);
		suchbegriff = suchbegriff.replaceAll(" ", "%20");
		return GOOGLE + suchbegriff;
	}

	public void printSuchbegriffe() {
		// FIXME: hier ein File erzeugen.
		for (String s : generierteSuchbegriffe) {
			System.out.println(s + "\n");
		}
	}
}
