/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author heike
 *
 */
public class AufgabeErgebnisTest {

	@Test
	public void test_konstruktor_soll_beim_setzen_des_index_soll_auch_die_nummer_setzen() {

		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(0, 0, 0, 0);
			assertEquals("A-1", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(1, 0, 0, 0);
			assertEquals("A-2", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(2, 0, 0, 0);
			assertEquals("A-3", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(3, 0, 0, 0);
			assertEquals("A-4", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(4, 0, 0, 0);
			assertEquals("A-5", ergebnis.getNummer());
		}

		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(5, 0, 0, 0);
			assertEquals("B-1", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(6, 0, 0, 0);
			assertEquals("B-2", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(7, 0, 0, 0);
			assertEquals("B-3", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(8, 0, 0, 0);
			assertEquals("B-4", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(9, 0, 0, 0);
			assertEquals("B-5", ergebnis.getNummer());
		}

		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(10, 0, 0, 0);
			assertEquals("C-1", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(11, 0, 0, 0);
			assertEquals("C-2", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(12, 0, 0, 0);
			assertEquals("C-3", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(13, 0, 0, 0);
			assertEquals("C-4", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis(14, 0, 0, 0);
			assertEquals("C-5", ergebnis.getNummer());
		}
	}

	@Test
	public void test_setter_soll_beim_setzen_des_index_soll_auch_die_nummer_setzen() {

		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(0);
			assertEquals("A-1", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(1);
			assertEquals("A-2", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(2);
			assertEquals("A-3", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(3);
			assertEquals("A-4", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(4);
			assertEquals("A-5", ergebnis.getNummer());
		}

		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(5);
			assertEquals("B-1", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(6);
			assertEquals("B-2", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(7);
			assertEquals("B-3", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(8);
			assertEquals("B-4", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(9);
			assertEquals("B-5", ergebnis.getNummer());
		}

		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(10);
			assertEquals("C-1", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(11);
			assertEquals("C-2", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(12);
			assertEquals("C-3", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(13);
			assertEquals("C-4", ergebnis.getNummer());
		}
		{
			AufgabeErgebnis ergebnis = new AufgabeErgebnis();
			ergebnis.setIndex(14);
			assertEquals("C-5", ergebnis.getNummer());
		}
	}
}
