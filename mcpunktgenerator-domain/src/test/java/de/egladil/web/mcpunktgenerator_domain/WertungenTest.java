/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import junit.framework.TestCase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heike
 */
public class WertungenTest extends TestCase {

  private static final Logger LOG = LoggerFactory.getLogger(WertungenTest.class);

  public void testTransformToRichtig() {
	assertEquals(Wertung.RICHTIG, Wertung.transform("r"));
	assertEquals(Wertung.RICHTIG, Wertung.transform("R"));
  }

  public void testTransformToFalsch() {
	assertEquals(Wertung.FALSCH, Wertung.transform("f"));
	assertEquals(Wertung.FALSCH, Wertung.transform("F"));
  }

  public void testTransformToLeer() {
	assertEquals(Wertung.LEER, Wertung.transform(" "));
	assertEquals(Wertung.LEER, Wertung.transform("n"));
	assertEquals(Wertung.LEER, Wertung.transform("N"));
	assertEquals(Wertung.LEER, Wertung.transform("0"));
  }

  public void testTransformLeeresKuerzel() {
	try {
	  Wertung.transform("");
	  fail("keine IllegalArgumentException");
	} catch (IllegalArgumentException e) {
	  LOG.info(e.getMessage());
	}
  }

  public void testTransformNullKuerzel() {
	try {
	  Wertung.transform(null);
	  fail("keine IllegalArgumentException");
	} catch (IllegalArgumentException e) {
	  LOG.info(e.getMessage());
	}
  }
}
