/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import junit.framework.TestCase;

/**
 * @author heike
 */
public class GesamtpunktverteilungItemTest extends TestCase {


  /*
   * (non-Javadoc)
   * @see junit.framework.TestCase#setUp()
   */
  protected void setUp() throws Exception {
	super.setUp();
  }

  public void testCompare() {
	GesamtpunktverteilungItem item1 = new GesamtpunktverteilungItem();
	item1.setPunkte("25.0-29.95");
	GesamtpunktverteilungItem item2 = new GesamtpunktverteilungItem();
	item2.setPunkte("30.0-34.95");
	GesamtpunktverteilungItem item3 = new GesamtpunktverteilungItem();
	item3.setPunkte("35.0-39.95");
	assertEquals(0, item1.compareTo(item1));
	assertEquals(0, item2.compareTo(item2));
	assertEquals(0, item3.compareTo(item3));
	assertTrue("item1 nicht groesser item2", item1.compareTo(item2) > 0);
	assertTrue("item2 nicht groesser item3", item2.compareTo(item3) > 0);
	assertTrue("item1 nicht groesser", item1.compareTo(item3) > 0);

	assertTrue("item1 nicht kleiner item2", item2.compareTo(item1) < 0);
	assertTrue("item3 nicht kleiner item2", item3.compareTo(item2) < 0);
	assertTrue("item3 nicht kleiner item1", item3.compareTo(item1) < 0);
  }

  public void testGetPunktintervall() {
	GesamtpunktverteilungItem item = new GesamtpunktverteilungItem();
	item.setPunkte("25.0-29.95");
	Punktintervall intervall = item.getPunktIntervall();
	assertNotNull(intervall);
	assertEquals(25.0, intervall.getMinVal());
	assertEquals(29.95, intervall.getMaxVal());
  }
}
