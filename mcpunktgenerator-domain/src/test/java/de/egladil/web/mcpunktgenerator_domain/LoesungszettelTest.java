/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

/**
 * @author heike
 */
public class LoesungszettelTest extends TestCase {

  private Loesungszettel loesungszettel;

  /*
   * (non-Javadoc)
   * @see junit.framework.TestCase#setUp()
   */
  protected void setUp() throws Exception {
	super.setUp();
  }

  public void testBerechneGesamtpunktzahlNormal() {
	List<IEinzelwertung> einzelwertungen = createEinzelwertungen();
	loesungszettel = new Loesungszettel(einzelwertungen);
	double punkte = loesungszettel.berechneGesamtpunktzahl();
	assertEquals(25.25, punkte);
  }

  public void testBerechneGuthabenNormal() {
	List<IEinzelwertung> einzelwertungen = createEinzelwertungen();
	loesungszettel = new Loesungszettel(einzelwertungen);
	double punkte = loesungszettel.berechneStartguthaben();
	assertEquals(15.0, punkte);
  }

  private List<IEinzelwertung> createEinzelwertungen(){
	List<IEinzelwertung> result = new ArrayList<IEinzelwertung>();
	result.add(new Einzelwertung(new Loesung("1", Aufgabenkategorie.DREI), Wertung.FALSCH));
	result.add(new Einzelwertung(new Loesung("2", Aufgabenkategorie.DREI), Wertung.FALSCH));
	result.add(new Einzelwertung(new Loesung("3", Aufgabenkategorie.DREI), Wertung.RICHTIG));
	result.add(new Einzelwertung(new Loesung("4", Aufgabenkategorie.DREI), Wertung.RICHTIG));
	result.add(new Einzelwertung(new Loesung("5", Aufgabenkategorie.DREI), Wertung.LEER));

	result.add(new Einzelwertung(new Loesung("6", Aufgabenkategorie.VIER), Wertung.LEER));
	result.add(new Einzelwertung(new Loesung("7", Aufgabenkategorie.VIER), Wertung.FALSCH));
	result.add(new Einzelwertung(new Loesung("8", Aufgabenkategorie.VIER), Wertung.FALSCH));
	result.add(new Einzelwertung(new Loesung("9", Aufgabenkategorie.VIER), Wertung.RICHTIG));
	result.add(new Einzelwertung(new Loesung("10", Aufgabenkategorie.VIER), Wertung.LEER));

	result.add(new Einzelwertung(new Loesung("11", Aufgabenkategorie.FUENF), Wertung.LEER));
	result.add(new Einzelwertung(new Loesung("12", Aufgabenkategorie.FUENF), Wertung.RICHTIG));
	result.add(new Einzelwertung(new Loesung("13", Aufgabenkategorie.FUENF), Wertung.FALSCH));
	result.add(new Einzelwertung(new Loesung("14", Aufgabenkategorie.FUENF), Wertung.LEER));
	result.add(new Einzelwertung(new Loesung("15", Aufgabenkategorie.FUENF), Wertung.LEER));
	return result;
  }

  public void testGehoertZuIntervall(){
	Punktintervall intervall = new Punktintervall(25.0, 29.95);
	List<IEinzelwertung> einzelwertungen = createEinzelwertungen();
	loesungszettel = new Loesungszettel(einzelwertungen);
	assertTrue(loesungszettel.gehoertZuPunktintervall(intervall));

	intervall = new Punktintervall(20.0, 24.95);
	assertFalse(loesungszettel.gehoertZuPunktintervall(intervall));
  }
}
