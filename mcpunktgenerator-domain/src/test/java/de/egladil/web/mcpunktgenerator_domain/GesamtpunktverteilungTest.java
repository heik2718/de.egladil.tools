/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.io.IOUtils;

import junit.framework.TestCase;

/**
 * @author heike
 */
public class GesamtpunktverteilungTest extends TestCase {

	private JAXBContext jaxbContext;

	/*
	 * (non-Javadoc)
	 *
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		jaxbContext = JAXBContext.newInstance(Gesamtpunktverteilung.class, GesamtpunktverteilungItem.class);
	}

	public void testMarshal() throws IOException, JAXBException {
		final Gesamtpunktverteilung verteilung = new Gesamtpunktverteilung();
		verteilung.setTitel("Auswertung Minigänguru 2012");
		verteilung.setBasis(
			"Die folgende Punktverteilung wurde auf der Grundlage der zur\u00fcckgesendeten Ergebnisse von 50 Schulen ermittelt.");
		verteilung.setBewertung(
			"Die Kinder erhielten ein Startguthaben von 15,00 Punkten. F\u00fcr falsch gel\u00f6ste Aufgaben wurden abgezogen: 0,75 Punkte bei 3-Punkte-Aufgaben, 1 Punkt bei 4-Punkte-Aufgaben, 1,25 Punkte bei 5-Punkte-Aufgaben.");
		GesamtpunktverteilungItem item = new GesamtpunktverteilungItem();
		item.setPunkte("0.0-4.95");
		item.setAnzahl(7);
		item.setProzentrang("1,64");
		verteilung.addItem(item);
		item = new GesamtpunktverteilungItem();
		item.setPunkte("5.0-9.95");
		item.setAnzahl(11);
		item.setProzentrang("4,21");
		verteilung.addItem(item);
		item = new GesamtpunktverteilungItem();
		item.setPunkte("10.0-14.95");
		item.setAnzahl(32);
		item.setProzentrang("11,68");
		verteilung.addItem(item);

		verteilung.setMedian("22.75");

		final List<AufgabeErgebnis> ergebnisse = new ArrayList<AufgabeErgebnis>();
		ergebnisse.add(new AufgabeErgebnis(0, 1500, 85, 1593));
		ergebnisse.add(new AufgabeErgebnis(10, 123, 1500, 555));
		verteilung.setEinzelergebnisse(ergebnisse);

		final StringWriter writer = new StringWriter();
		jaxbContext.createMarshaller().marshal(verteilung, writer);
		System.out.println(writer.toString());
	}

	private Gesamtpunktverteilung loadVerteilung() {
		InputStream in = null;
		in = getClass().getResourceAsStream("/gesamtpunktverteilung.xml");
		Gesamtpunktverteilung result;
		try {
			result = (Gesamtpunktverteilung) jaxbContext.createUnmarshaller().unmarshal(in);
			return result;
		} catch (final JAXBException e) {
			e.printStackTrace();
			return null;
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	public void testBerechneGesamtzahlTeilnehmer() {
		final Gesamtpunktverteilung verteilung = loadVerteilung();
		assertNotNull(verteilung);
		final int anzahlTeilnehmer = verteilung.berechneGesamtzahlTeilnehmer();
		assertEquals(118, anzahlTeilnehmer);
	}

	public void testAnzahlTeilnehmerMitPunktklasseGleichOderBesser() {
		final Gesamtpunktverteilung verteilung = loadVerteilung();
		assertNotNull(verteilung);
		Punktintervall referenzItervall = new Punktintervall(45.0, 49.95);
		int anzahl = verteilung.anzahlTeilnehmerMitPunktenGleichOderBesser(referenzItervall);
		assertEquals(13, anzahl);

		referenzItervall = new Punktintervall(35.0, 39.95);
		anzahl = verteilung.anzahlTeilnehmerMitPunktenGleichOderBesser(referenzItervall);
		assertEquals(34, anzahl);
	}

	public void testAnzahlTeilnehmerMitPunktenGleichOderSchlechter() {
		final Gesamtpunktverteilung verteilung = loadVerteilung();
		assertNotNull(verteilung);
		Punktintervall referenzItervall = new Punktintervall(45.0, 49.95);
		int anzahl = verteilung.anzahlTeilnehmerMitPunktenGleichOderSchlechter(referenzItervall);
		assertEquals(109, anzahl);

		referenzItervall = new Punktintervall(35.0, 39.95);
		anzahl = verteilung.anzahlTeilnehmerMitPunktenGleichOderSchlechter(referenzItervall);
		assertEquals(97, anzahl);
	}

	public void testBerechneProzentraenge() {
		final Gesamtpunktverteilung verteilung = loadVerteilung();
		assertNotNull(verteilung);
		assertFalse(verteilung.isKannGeneriertWerden());
		verteilung.berechneProzentraenge();
		assertTrue(verteilung.isKannGeneriertWerden());
		assertEquals(236, verteilung.getAnzahlTeilnehmer());
		for (final GesamtpunktverteilungItem item : verteilung.getItemsSorted()) {
			System.out.println(item.toString());
		}
		// int index = 0;
		// for (GesamtpunktverteilungItem item : verteilung.getItemsSorted()) {
		// switch (index) {
		// case 0:
		// assertEquals(100.0, item.getProzentrang());
		// break;
		// case 1:
		// assertEquals(100.0, item.getProzentrang());
		// break;
		// case 2:
		// assertEquals(99.77, item.getProzentrang());
		// break;
		// case 3:
		// assertEquals(98.83, item.getProzentrang());
		// break;
		// case 4:
		// assertEquals(96.5, item.getProzentrang());
		// break;
		// case 5:
		// assertEquals(93.93, item.getProzentrang());
		// break;
		// case 6:
		// assertEquals(89.95, item.getProzentrang());
		// break;
		// case 7:
		// assertEquals(84.35, item.getProzentrang());
		// break;
		// case 8:
		// assertEquals(76.17, item.getProzentrang());
		// break;
		// case 9:
		// assertEquals(64.72, item.getProzentrang());
		// break;
		// case 10:
		// assertEquals(52.1, item.getProzentrang());
		// break;
		// case 11:
		// assertEquals(38.79, item.getProzentrang());
		// break;
		// case 12:
		// assertEquals(23.36, item.getProzentrang());
		// break;
		// case 13:
		// assertEquals(11.68, item.getProzentrang());
		// break;
		// case 14:
		// assertEquals(4.21, item.getProzentrang());
		// break;
		// case 15:
		// assertEquals(1.64, item.getProzentrang());
		// break;
		// default:
		// break;
		// }
		// index++;
		// }
	}
}
