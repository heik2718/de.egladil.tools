/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * @author heike
 */
public class ZeilenumwandlerTest extends TestCase {

  private static final Logger LOG = LoggerFactory.getLogger(ZeilenumwandlerTest.class);

  private Zeilenumwandler zeilenumwandler = new Zeilenumwandler();

  public void testZeileUngueltigNull() {
	String zeile = null;
	try {
	  zeilenumwandler.umwandeln(zeile);
	  fail("keine IllegalArgumentException");
	} catch (IllegalArgumentException e) {
	  LOG.info(e.getMessage());
	}
  }

  public void testZeileUngueltigZuKurz() {
	String zeile = "f;f;r;f;n;n;f;r;r;f;f;r;f;n";
	try {
	  zeilenumwandler.umwandeln(zeile);
	  fail("keine IllegalArgumentException");
	} catch (IllegalArgumentException e) {
	  LOG.info(e.getMessage());
	}
  }

  public void testZeileUngueltigZuLang() {
	String zeile = "f;f;r;f;n;n;f;r;r; ;f;r;f;n;r;r";
	try {
	  zeilenumwandler.umwandeln(zeile);
	  fail("keine IllegalArgumentException");
	} catch (IllegalArgumentException e) {
	  LOG.info(e.getMessage());
	}
  }

  public void testSuccess() {
	String zeile = "r;n;f;f;n;f;n;r;r;f;f;f;n;f;f";
	Loesungszettel loesungszettel = zeilenumwandler.umwandeln(zeile);
	assertNotNull(loesungszettel);
	List<IEinzelwertung> einzelwertungen = loesungszettel.getEinzelwertungen();
	List<Integer> richtige = Arrays.asList(new Integer[] { 0, 7, 8 });
	List<Integer> falsche = Arrays.asList(new Integer[] { 2, 3, 5, 9, 10, 11, 13, 14 });
	List<Integer> leere = Arrays.asList(new Integer[] { 1, 4, 6, 12 });
	for (int i = 0; i < einzelwertungen.size(); i++) {
	  if (richtige.contains(i)) {
		IEinzelwertung ew = einzelwertungen.get(i);
		assertEquals("Fehler bei i=" + i, Wertung.RICHTIG, ew.getWertung());
		assertNotNull(ew.getAufgabenkategorie());
	  }
	  if (falsche.contains(i)) {
		IEinzelwertung ew = einzelwertungen.get(i);
		assertEquals("Fehler bei i=" + i, Wertung.FALSCH, ew.getWertung());
		assertNotNull(ew.getAufgabenkategorie());
	  }
	  if (leere.contains(i)) {
		IEinzelwertung ew = einzelwertungen.get(i);
		assertEquals("Fehler bei i=" + i, Wertung.LEER, ew.getWertung());
		assertNotNull(ew.getAufgabenkategorie());
	  }
	}
  }
}
