/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.List;

import junit.framework.TestCase;

/**
 * @author heike
 */
public class PunktintervallTest extends TestCase {

  private Punktintervall interval;

  /*
   * (non-Javadoc)
   * @see junit.framework.TestCase#setUp()
   */
  protected void setUp() throws Exception {
	super.setUp();
  }

  public void testConstructor() {
	final double minVal = 35;
	final double maxVal = 39.95;
	interval = new Punktintervall(minVal, maxVal);
	assertEquals(minVal, interval.getMinVal());
	assertEquals(maxVal, interval.getMaxVal());
	assertTrue(interval.contains(minVal));
	assertTrue(interval.contains(maxVal));
	assertFalse(interval.contains(34.99));
	assertFalse(interval.contains(39.96));
	assertTrue(interval.contains(37.75));
  }

  public void testEquals() {
	final double minVal = 35;
	final double maxVal = 39.95;
	interval = new Punktintervall(minVal, maxVal);
	Punktintervall other = new Punktintervall(minVal, maxVal);
	assertEquals(interval, other);
  }

  public void testMinikaengPunktintervalle() {
	List<Punktintervall> punktintervalle = Punktintervall.allMinikaenguruPunktintervalleDescending();
	assertNotNull(punktintervalle);
	assertEquals(16, punktintervalle.size());
//	assertTrue(punktintervalle.contains(new Punktintervall(75.0, 75.0)));
//	assertTrue(punktintervalle.contains(new Punktintervall(70.0, 74.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(65.0, 69.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(60.0, 64.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(55.0, 59.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(50.0, 54.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(45.0, 49.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(40.0, 44.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(35.0, 39.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(30.0, 34.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(25.0, 29.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(20.0, 24.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(15.0, 19.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(10.0, 14.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(5.0, 9.95)));
//	assertTrue(punktintervalle.contains(new Punktintervall(0.0, 4.95)));
	assertTrue(punktintervalle.contains(new Punktintervall(75.0, 75.0)));
	assertTrue(punktintervalle.contains(new Punktintervall(70.0, 72.0)));
	assertTrue(punktintervalle.contains(new Punktintervall(65.0, 69.0)));
	assertTrue(punktintervalle.contains(new Punktintervall(60.0, 64.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(55.0, 59.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(50.0, 54.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(45.0, 49.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(40.0, 44.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(35.0, 39.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(30.0, 34.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(25.0, 29.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(20.0, 24.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(15.0, 19.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(10.0, 14.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(5.0, 9.75)));
	assertTrue(punktintervalle.contains(new Punktintervall(0.0, 4.75)));

	Punktintervall erstes = punktintervalle.get(0);
	Punktintervall zweites = punktintervalle.get(1);
	assertEquals(-1, erstes.compareTo(zweites));

	for (Punktintervall intervall : punktintervalle){
	  System.out.println(intervall.toString());
	}
  }

  public void testCompare(){
	Punktintervall intervall1 = new Punktintervall(70.0, 74.95);
	Punktintervall intervall2 = new Punktintervall(75.0, 75.0);
	assertTrue(intervall1.compareTo(intervall2) > 0);
	assertEquals(0, intervall1.compareTo(intervall1));
	assertEquals(0, intervall2.compareTo(intervall2));
	assertTrue(intervall2.compareTo(intervall1) < 0);
  }
}
