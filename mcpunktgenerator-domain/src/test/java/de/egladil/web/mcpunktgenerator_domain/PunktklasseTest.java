/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;

/**
 * @author heike
 */
public class PunktklasseTest extends TestCase {

  private static final Logger LOG = LoggerFactory.getLogger(PunktklasseTest.class);

  /*
   * (non-Javadoc)
   * @see junit.framework.TestCase#setUp()
   */
  protected void setUp() throws Exception {
	super.setUp();
  }

  public void testConstructorParamNull() {
	try {
	  new Punktintervallklasse(null);
	  fail("keine IllegalArgumentException");
	} catch (IllegalArgumentException e) {
	  LOG.info(e.getMessage());
	}
  }

  public void testConstructorNormal() {
	Punktintervall interval = new Punktintervall(35, 39.95);
	Punktintervallklasse klasse = new Punktintervallklasse(interval);
	assertNotNull(klasse.getInterval());
	assertEquals(klasse.getInterval(), interval);
	assertEquals(0, klasse.getCardinality());
  }

  /**
   *
   */
  public void testAddElement() {
	Punktintervall interval = new Punktintervall(35, 39.95);
	Punktintervallklasse klasse = new Punktintervallklasse(interval);
	klasse.addLoesungszettel(null);
	assertEquals(1, klasse.getCardinality());
  }
}
