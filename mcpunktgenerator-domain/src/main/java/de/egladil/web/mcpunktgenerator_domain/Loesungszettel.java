/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Virtueller Loesungszettel eines Kindes. Standardmäßig werden 15 Aufgaben (Minikänguru) zugrundegelegt.
 *
 * @author heike
 */
public class Loesungszettel {

	public static final int MINIKAENGURU_AUFGABENANZAHL = 15;

	private final List<IEinzelwertung> einzelwertungen;

	/**
	 *
	 * Factorymethode ohne Validierungen.
	 *
	 * @param aufgabennummernKategorie3
	 * @param wertungenKategorie3
	 * @param aufgabennummernKategorie4
	 * @param wertungenKategorie4
	 * @param aufgabennummernKategorie5
	 * @param wertungenKategorie5
	 * @return
	 */
	public static Loesungszettel create(final String[] aufgabennummernKategorie3, final Wertung[] wertungenKategorie3,
		final String[] aufgabennummernKategorie4, final Wertung[] wertungenKategorie4, final String[] aufgabennummernKategorie5,
		final Wertung[] wertungenKategorie5) {

		final Loesungszettel loesungszettel = new Loesungszettel();

		for (int i = 0; i < aufgabennummernKategorie3.length; i++) {
			final Loesung loesung = new Loesung(aufgabennummernKategorie3[i], Aufgabenkategorie.DREI);
			final Einzelwertung einzelauswertung = new Einzelwertung(loesung, wertungenKategorie3[i]);
			loesungszettel.addEinzelauswertung(einzelauswertung);
		}
		for (int i = 0; i < aufgabennummernKategorie4.length; i++) {
			final Loesung loesung = new Loesung(aufgabennummernKategorie4[i], Aufgabenkategorie.VIER);
			final Einzelwertung einzelauswertung = new Einzelwertung(loesung, wertungenKategorie4[i]);
			loesungszettel.addEinzelauswertung(einzelauswertung);
		}
		for (int i = 0; i < aufgabennummernKategorie5.length; i++) {
			final Loesung loesung = new Loesung(aufgabennummernKategorie5[i], Aufgabenkategorie.FUENF);
			final Einzelwertung einzelauswertung = new Einzelwertung(loesung, wertungenKategorie5[i]);
			loesungszettel.addEinzelauswertung(einzelauswertung);
		}

		return loesungszettel;

	}

	/**
	 * Erzeugt eine Instanz von Loesungszettel
	 */
	Loesungszettel() {
		this.einzelwertungen = new ArrayList<>();
	}

	void addEinzelauswertung(final IEinzelwertung einzelauswertung) {
		this.einzelwertungen.add(einzelauswertung);
	}

	/**
	 * @param pEinzelwertungen
	 */
	public Loesungszettel(final List<IEinzelwertung> pEinzelwertungen) {
		this(pEinzelwertungen, MINIKAENGURU_AUFGABENANZAHL);
	}

	/**
	 * @param pEinzelwertungen
	 */
	public Loesungszettel(final List<IEinzelwertung> pEinzelwertungen, final int pAnzahl) {
		super();
		final int aufgabenanzahl = pAnzahl;
		if (pEinzelwertungen == null || pEinzelwertungen.size() != aufgabenanzahl) {
			throw new IllegalArgumentException("die Einzelwertungen duerfen nicht null sein und muessen genau "
				+ aufgabenanzahl + " Elemente enthalten");
		}
		einzelwertungen = pEinzelwertungen;
	}

	/**
	 * Die Reihenfolge entspricht der Reihenfolge der Wettbewerbsaufgaben (also 3-1, 3-2, ..., 5-5)
	 *
	 * @return the einzelwertungen (unmodifiable)
	 */
	public List<IEinzelwertung> getEinzelwertungen() {
		return Collections.unmodifiableList(einzelwertungen);
	}

	/**
	 * Berechnet die Gesamtpunktzahl.
	 *
	 * @return double
	 */
	public double berechneGesamtpunktzahl() {
		final double guthaben = berechneStartguthaben();
		double punktzahl = 0.0;
		for (final IEinzelwertung einzelwertung : einzelwertungen) {
			final Aufgabenkategorie kategorie = einzelwertung.getAufgabenkategorie();
			final Wertung wertung = einzelwertung.getWertung();
			switch (wertung) {
			case FALSCH:
				punktzahl -= kategorie.getStrafpunkte(einzelwertung.getAnzahlAntwortvorschlaege());
				break;
			case RICHTIG:
				punktzahl += kategorie.getPunkte();
			default:
				break;
			}
		}
		return punktzahl + guthaben;
	}

	/**
	 * Berechnet das Startguthaben.
	 *
	 * @return double
	 */
	public double berechneStartguthaben() {
		double guthaben = 0.0;
		for (final IEinzelwertung wertung : einzelwertungen) {
			final Aufgabenkategorie kategorie = wertung.getAufgabenkategorie();
			guthaben += kategorie.getStrafpunkte(wertung.getAnzahlAntwortvorschlaege());
		}
		return guthaben;
	}

	/**
	 * Der Lösungszettel gehört zum gegebenen Punktintervall, wenn die Gesamtpunktzahl im Inneren des abgschlossenen
	 * Intervalls liegt.
	 *
	 * @param pIntervall
	 * @return
	 */
	public boolean gehoertZuPunktintervall(final Punktintervall pIntervall) {
		final double gesamtpunkte = berechneGesamtpunktzahl();
		return pIntervall.getMinVal() <= gesamtpunkte && gesamtpunkte <= pIntervall.getMaxVal();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		final double punkte = this.berechneGesamtpunktzahl();
		sb.append(" Punkte: " + punkte);
		sb.append(" - ");
		for (final IEinzelwertung einzelwertung : einzelwertungen){
			sb.append(einzelwertung.getWertung());
			sb.append(" ");
		}
		// for (IEinzelwertung einzelwertung : einzelwertungen) {
		// sb.append(einzelwertung.toString());
		// }
		return sb.toString();
	}
}
