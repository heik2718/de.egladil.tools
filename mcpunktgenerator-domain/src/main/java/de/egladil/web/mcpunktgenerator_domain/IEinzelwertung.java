/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

/**
 * Eine Einzelwertung.
 *
 * @author heike
 */
public interface IEinzelwertung {

  /**
   * @return Aufgabenkategorien
   */
  Aufgabenkategorie getAufgabenkategorie();

  /**
   * @return the wertung
   */
  Wertung getWertung();

  /**
   *
   * @return gibt die Anzahl der Antwortvorschläge für diese Aufgabe zurück.
   */
  int getAnzahlAntwortvorschlaege();
}
