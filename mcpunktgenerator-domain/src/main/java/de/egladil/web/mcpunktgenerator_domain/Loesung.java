/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;


/**
 * Eine Lösung des Wettbewerbs.
 *
 * @author heike
 */
public class Loesung {

  private final String aufgabennummer;

  private final Aufgabenkategorie kategorie;



  /**
   * @param pAufgabennummer
   * @param pKategorie
   */
  public Loesung(String pAufgabennummer, Aufgabenkategorie pKategorie) {
	super();
	aufgabennummer = pAufgabennummer;
	kategorie = pKategorie;
  }

  /**
   * @return the aufgabennummer
   */
  public final String getAufgabennummer() {
    return aufgabennummer;
  }

  /**
   * @return the kategorie
   */
  public final Aufgabenkategorie getKategorie() {
    return kategorie;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((aufgabennummer == null) ? 0 : aufgabennummer.hashCode());
	return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
	if (this == obj)
	  return true;
	if (obj == null)
	  return false;
	if (getClass() != obj.getClass())
	  return false;
	Loesung other = (Loesung) obj;
	if (aufgabennummer == null) {
	  if (other.aufgabennummer != null)
		return false;
	} else if (!aufgabennummer.equals(other.aufgabennummer))
	  return false;
	return true;
  }

/**
* @see java.lang.Object#toString()
*/
@Override
public String toString() {
	return "Loesung [aufgabennummer=" + aufgabennummer + ", kategorie=" + kategorie + "]";
}
}
