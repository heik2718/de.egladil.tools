/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

/**
 * Bewertung einer Lösung.
 *
 * @author heike
 */
public enum Wertung {

  RICHTIG, FALSCH, LEER;

  /**
   * Transformiert ein Kürzel in die Wertung.
   *
   * @param pKuerzel
   * @return Wertungen
   */
  public static Wertung transform(String pKuerzel) {
	if(pKuerzel == null || pKuerzel.isEmpty()){
	  throw new IllegalArgumentException("pKuerzel darf nicht null und nicht leer sein.");
	}
	if ("r".equalsIgnoreCase(pKuerzel)) {
	  return RICHTIG;
	}
	if ("f".equalsIgnoreCase(pKuerzel)) {
	  return FALSCH;
	}
	return LEER;
  }
}
