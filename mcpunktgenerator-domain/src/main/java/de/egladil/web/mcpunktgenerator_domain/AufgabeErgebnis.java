/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author heike
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AufgabeErgebnis implements Comparable<AufgabeErgebnis> {

	@XmlElement(name = "nummer")
	private String nummer;

	@XmlTransient
	private int index;

	@XmlElement(name = "anzahlRichtigGeloest")
	private int anzahlRichtigGeloest;

	@XmlElement(name = "anteilRichtigGeloest")
	private String anteilRichtigGeloest;

	@XmlElement(name = "anzahlFalschGeloest")
	private int anzahlFalschGeloest;

	@XmlElement(name = "anteilFalschGeloest")
	private String anteilFalschGeloest;

	@XmlElement(name = "anzahlNichtGeloest")
	private int anzahlNichtGeloest;

	@XmlElement(name = "anteilNichtGeloest")
	private String anteilNichtGeloest;

	/**
	 *
	 */
	public AufgabeErgebnis() {
	}

	/**
	 * @param index
	 * @param anzahlRichtigGeloest
	 * @param anzahlNichtGeloest
	 * @param anzahlFalschGeloest
	 */
	public AufgabeErgebnis(final int index, final int anzahlRichtigGeloest, final int anzahlNichtGeloest,
		final int anzahlFalschGeloest) {
		super();
		this.setIndex(index);
		this.anzahlRichtigGeloest = anzahlRichtigGeloest;
		this.anzahlNichtGeloest = anzahlNichtGeloest;
		this.anzahlFalschGeloest = anzahlFalschGeloest;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final AufgabeErgebnis arg0) {
		return this.index - arg0.getIndex();
	}

	private String getAufgabennummer(final int index) {
		switch (index) {
		case 0:
			return "A-1";
		case 1:
			return "A-2";
		case 2:
			return "A-3";
		case 3:
			return "A-4";
		case 4:
			return "A-5";
		case 5:
			return "B-1";
		case 6:
			return "B-2";
		case 7:
			return "B-3";
		case 8:
			return "B-4";
		case 9:
			return "B-5";
		case 10:
			return "C-1";
		case 11:
			return "C-2";
		case 12:
			return "C-3";
		case 13:
			return "C-4";
		case 14:
			return "C-5";
		default:
			break;
		}
		throw new IllegalArgumentException("Index muss zwischen 0 und 14 liegen!");
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(final int index) {
		this.index = index;
		this.nummer = getAufgabennummer(this.index);
	}

	/**
	 * @return the anzahlRichtigGeloest
	 */
	public int getAnzahlRichtigGeloest() {
		return anzahlRichtigGeloest;
	}

	/**
	 * @param anzahlRichtigGeloest the anzahlRichtigGeloest to set
	 */
	public void setAnzahlRichtigGeloest(final int anzahlRichtigGeloest) {
		this.anzahlRichtigGeloest = anzahlRichtigGeloest;
	}

	/**
	 * @return the anzahlNichtGeloest
	 */
	public int getAnzahlNichtGeloest() {
		return anzahlNichtGeloest;
	}

	/**
	 * @param anzahlNichtGeloest the anzahlNichtGeloest to set
	 */
	public void setAnzahlNichtGeloest(final int anzahlNichtGeloest) {
		this.anzahlNichtGeloest = anzahlNichtGeloest;
	}

	/**
	 * @return the anzahlFalschGeloest
	 */
	public int getAnzahlFalschGeloest() {
		return anzahlFalschGeloest;
	}

	/**
	 * @param anzahlFalschGeloest the anzahlFalschGeloest to set
	 */
	public void setAnzahlFalschGeloest(final int anzahlFalschGeloest) {
		this.anzahlFalschGeloest = anzahlFalschGeloest;
	}

	// /**
	// * @return the anteilRichtigGeloest
	// */
	// public double getAnteilRichtigGeloest() {
	// return anteilRichtigGeloest;
	// }
	//
	// /**
	// * @param anteilRichtigGeloest the anteilRichtigGeloest to set
	// */
	// public void setAnteilRichtigGeloest(final double anteilRichtigGeloest) {
	// this.anteilRichtigGeloest = anteilRichtigGeloest;
	// }
	//
	// /**
	// * @return the anteilNichtGeloest
	// */
	// public double getAnteilNichtGeloest() {
	// return anteilNichtGeloest;
	// }
	//
	// /**
	// * @param anteilNichtGeloest the anteilNichtGeloest to set
	// */
	// public void setAnteilNichtGeloest(final double anteilNichtGeloest) {
	// this.anteilNichtGeloest = anteilNichtGeloest;
	// }
	//
	// /**
	// * @return the anteilFalschGeloest
	// */
	// public double getAnteilFalschGeloest() {
	// return anteilFalschGeloest;
	// }
	//
	// /**
	// * @param anteilFalschGeloest the anteilFalschGeloest to set
	// */
	// public void setAnteilFalschGeloest(final double anteilFalschGeloest) {
	// this.anteilFalschGeloest = anteilFalschGeloest;
	// }

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AufgabeErgebnis [nummer=" + nummer + ", anzahlRichtigGeloest=" + anzahlRichtigGeloest + ", anzahlNichtGeloest="
			+ anzahlNichtGeloest + ", anzahlFalschGeloest=" + anzahlFalschGeloest + ", anteilRichtigGeloest=" + anteilRichtigGeloest
			+ ", anteilNichtGeloest=" + anteilNichtGeloest + ", anteilFalschGeloest=" + anteilFalschGeloest + "]";
	}

	/**
	 * @return the nummer
	 */
	public String getNummer() {
		return nummer;
	}

	/**
	 * @param nummer the nummer to set
	 */
	public void setNummer(final String nummer) {
		this.nummer = nummer;
	}

	public void setAnteilRichtigGeloest(final String anteilRichtigGeloest) {
		this.anteilRichtigGeloest = anteilRichtigGeloest;
	}

	public void setAnteilFalschGeloest(final String anteilFalschGeloest) {
		this.anteilFalschGeloest = anteilFalschGeloest;
	}

	public void setAnteilNichtGeloest(final String anteilNichtGeloest) {
		this.anteilNichtGeloest = anteilNichtGeloest;
	}

}
