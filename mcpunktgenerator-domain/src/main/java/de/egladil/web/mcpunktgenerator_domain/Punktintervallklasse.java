/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * Eine PunktintervallKlasse enthält alle Lösungszettel, deren Gesamtpunktzahl in der Punktintervall fallen.
 *
 * @author Winkelv
 */
public class Punktintervallklasse {

	@XmlElement(name = "interval")
	private final Punktintervall interval;

	private final List<Loesungszettel> loesungszettel = new ArrayList<Loesungszettel>();

	/**
	 * @param pInterval
	 */
	public Punktintervallklasse(Punktintervall pInterval) {
		if (pInterval == null) {
			throw new IllegalArgumentException("Parameter null ist nicht erlaubt!");
		}
		interval = pInterval;
	}

	/**
	 * Fügt einen Lösungszettel hinzu.
	 *
	 * @param pLoesungszettel Loesungszettel darf nicht null sein.
	 */
	public void addLoesungszettel(Loesungszettel pLoesungszettel) {
		loesungszettel.add(pLoesungszettel);
	}

	/**
	 * @return the interval
	 */
	public Punktintervall getInterval() {
		return interval;
	}

	/**
	 * Anzahl der Loesungszettel, die in diese Klasse fallen.
	 *
	 * @return the elements int die Anzahl der Elemente.
	 */
	public int getCardinality() {
		return loesungszettel.size();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DoublePunktklasse [interval=");
		builder.append(interval);
		builder.append(", cardinality=");
		builder.append(getCardinality());
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the loesungszettel unmodifiable
	 */
	public final List<Loesungszettel> getLoesungszettel() {
		return Collections.unmodifiableList(loesungszettel);
	}
}
