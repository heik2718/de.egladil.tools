//=====================================================
// Projekt: mcpunktgenerator-domain
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.web.mcpunktgenerator_domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Ein Rohpunktitem enthält die Punkte, die Anzahl der Lösungszettel mit dieser Punktzahl und den Prozentrang.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Rohpunktitem {

	@XmlElement(name = "punkte")
	private String punkteText;

	@XmlTransient
	private double punkte;

	@XmlElement(name = "anzahl")
	private int anzahl;

	@XmlTransient
	private int anzahlKumuliert;

	@XmlTransient
	private double prozentrang;

	@XmlElement(name = "prozentrang")
	private String prozentrangText;

	/**
	 * Erzeugt eine Instanz von Punktklasse
	 */
	public Rohpunktitem() {
		// Platzhalter
	}

	/**
	 * Erzeugt eine Instanz von Rohpunktitem
	 */
	public Rohpunktitem(double punkte, int anzahl) {
		super();
		this.setPunkte(punkte);
		this.anzahl = anzahl;
	}

	/**
	 * Liefert die Membervariable punkte
	 *
	 * @return die Membervariable punkte
	 */
	public String getPunkteText() {
		return punkteText;
	}

	/**
	 * Liefert die Membervariable anzahl
	 *
	 * @return die Membervariable anzahl
	 */
	public int getAnzahl() {
		return anzahl;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param anzahl neuer Wert der Membervariablen anzahl
	 */
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	/**
	 * Liefert die Membervariable prozentrang
	 *
	 * @return die Membervariable prozentrang
	 */
	public double getProzentrang() {
		return prozentrang;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param prozentrang neuer Wert der Membervariablen prozentrang
	 */
	public void setProzentrang(double prozentrang) {
		this.prozentrang = prozentrang;
		String text = String.format("%.3f", prozentrang);
		this.prozentrangText = text.replaceAll(",000", "");
	}

	/**
	 * Liefert die Membervariable anzahlKumuliert
	 *
	 * @return die Membervariable anzahlKumuliert
	 */
	public int getAnzahlKumuliert() {
		return anzahlKumuliert;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param anzahlKumuliert neuer Wert der Membervariablen anzahlKumuliert
	 */
	public void setAnzahlKumuliert(int anzahlKumuliert) {
		this.anzahlKumuliert = anzahlKumuliert;
	}

	/**
	 * Liefert die Membervariable punkte
	 *
	 * @return die Membervariable punkte
	 */
	public double getPunkte() {
		return punkte;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param punkte neuer Wert der Membervariablen punkte
	 */
	public void setPunkte(double punkte) {
		this.punkte = punkte;
		this.punkteText = punkte + "";
	}

}
