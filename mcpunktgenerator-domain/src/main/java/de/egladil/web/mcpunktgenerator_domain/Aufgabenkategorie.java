/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;


/**
 * @author heike
 */
public enum Aufgabenkategorie {

	DREI {
		@Override
		public int getPunkte() {
			return 3;
		}

		@Override
		public String getLabel() {
			return "3 Punkte";
		}

	},

	VIER {
		@Override
		public int getPunkte() {
			return 4;
		}

		@Override
		public String getLabel() {
			return "4 Punkte";
		}
	},

	FUENF {
		@Override
		public int getPunkte() {
			return 5;
		}

		@Override
		public String getLabel() {
			return "5 Punkte";
		}
	};

	/**
	 * @return
	 */
	public abstract String getLabel();

	/**
	 * Anzahl Punkte auf diese Aufgabe bei Wertung 'richtig'
	 *
	 * @return
	 */
	public abstract int getPunkte();

	/**
	 * Berechnet in Abhängigkeit der Antwortzahl die Strafpunkte.
	 *
	 * @param pAnzahlAntwortvorschlaege
	 * @return
	 */
	public double getStrafpunkte(int pAnzahlAntwortvorschlaege) {
		if (pAnzahlAntwortvorschlaege <= 1){
			throw new RuntimeException("Es muss mindestens 2 Antwortvorschläge geben!");
		}
		double zaehler = Double.valueOf("" + getPunkte());
		double nenner = Double.valueOf("" + pAnzahlAntwortvorschlaege) - 1;
		double result = zaehler / nenner;
		return result;
	}

	/**
	 * @param pInt
	 * @return
	 */
	public static Aufgabenkategorie valueOfInt(int pInt) {
		switch (pInt) {
		case 3:
			return DREI;
		case 4:
			return VIER;
		case 5:
			return FUENF;
		default:
			throw new IllegalArgumentException("Es sind nur 3, 4 und 5 erlaubt, war aber " + pInt);
		}
	}

	/**
	 * @return
	 */
	public static Aufgabenkategorie[] valuesSorted() {
		return new Aufgabenkategorie[] { DREI, VIER, FUENF };
	}
}