/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Ein Punktintervall ist das abgeschlossene Intervall [minVal, maxVal]. Es entspricht einer Zeile in der
 * Punktverteilung. Punktintervalle werden <b>absteigend</b> sortiert.
 *
 * @author Winkelv
 */
public class Punktintervall implements Comparable<Punktintervall> {

	private final double minVal;

	private final double maxVal;

	/**
	 * @param pMinVal
	 * @param pMaxVal
	 */
	public Punktintervall(double pMinVal, double pMaxVal) {
		super();
		minVal = pMinVal;
		maxVal = pMaxVal;
	}

	/**
	 * Wert ist enthalen, wenn er echt im gegebenen (abgeschlossenen) Interval enthalten ist.
	 *
	 * @param pWert
	 * @return
	 */
	public boolean contains(double pWert) {
		return minVal <= pWert && pWert <= maxVal;
	}

	/**
	 * @return the minVal
	 */
	public final double getMinVal() {
		return minVal;
	}

	/**
	 * @return the maxVal
	 */
	public final double getMaxVal() {
		return maxVal;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(maxVal);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(minVal);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punktintervall other = (Punktintervall) obj;
		if (Double.doubleToLongBits(maxVal) != Double.doubleToLongBits(other.getMaxVal()))
			return false;
		if (Double.doubleToLongBits(minVal) != Double.doubleToLongBits(other.getMinVal()))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Punktintervall pO) {
		if (this == pO) {
			return 0;
		}
		double paramMin = pO.getMinVal();
		if (minVal < paramMin) {
			return 1;
		}
		if (minVal == paramMin) {
			return 0;
		}
		return -1;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DoublePunktinterval [minVal=");
		builder.append(minVal);
		builder.append(", maxVal=");
		builder.append(maxVal);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return
	 */
	public String toGesamtpunktVerteilungString() {
		return minVal + "-" + maxVal;
	}

	/**
	 * Gibt alle Punktintervalle eines Minikänguruwettbewerbs in absteigender Reihenfolge zurück;
	 *
	 * @return {@link List}
	 */
	public static List<Punktintervall> allMinikaenguruPunktintervalleDescending() {
		List<Punktintervall> result = new ArrayList<Punktintervall>();
		double minimum = 0.0;
		double maximum = 0.0;
		for (int i = 0; i < 16; i++) {
			minimum = 5.0 * i;
			if (i != 15) {
				maximum = minimum + 4.75;
				// einige Punktzahlen gibt es nicht. Die werden hier entfernt
				if (maximum == 69.75){
					maximum = 69.0;
				}
				if (maximum == 74.75){
					maximum = 72.0;
				}
			} else {
				maximum = minimum;
			}
			result.add(new Punktintervall(minimum, maximum));
		}
		Collections.sort(result);
		return result;
	}
}
