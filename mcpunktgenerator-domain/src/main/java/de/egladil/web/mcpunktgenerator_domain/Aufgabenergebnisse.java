/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author heike
 *
 */
@XmlRootElement(name = "aufgabenergebnisse")
@XmlAccessorType(XmlAccessType.FIELD)
public class Aufgabenergebnisse {

	@XmlElement(name = "klasse")
	private String klasse;

	@XmlElement(name = "ergebnis")
	private List<AufgabeErgebnis> ergebnisse;

	/**
	 *
	 */
	public Aufgabenergebnisse() {
	}



	/**
	 * @return the klasse
	 */
	public String getKlasse() {
		return klasse;
	}

	/**
	 * @param klasse the klasse to set
	 */
	public void setKlasse(String klasse) {
		this.klasse = klasse;
	}

}
