/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


/**
 * Wandelt eine Semikolonseparierte Zeile in einen Loesungszettel um. Standardmäßig werden 15 Aufgaben zugrundegelegt.
 *
 * @author heike
 */
public class Zeilenumwandler {

  private static final int MINIKAENGURU_AUFGABENANZAHL = 15;

  private int tokenAnzahl;

  /**
   *
   */
  public Zeilenumwandler() {
	this(MINIKAENGURU_AUFGABENANZAHL);
  }

  /**
   * @param pTokenAnzahl
   */
  public Zeilenumwandler(int pTokenAnzahl) {
	super();
	tokenAnzahl = pTokenAnzahl;
  }

  /**
   * @param pZeile
   * @return
   */
  public Loesungszettel umwandeln(String pZeile) {
	if (pZeile == null) {
	  throw new IllegalArgumentException("pZeile darf nicht null sein");
	}
	List<IEinzelwertung> einzelwertungen = getEinzelwertungen(pZeile);
	return new Loesungszettel(einzelwertungen);
  }

  /**
   * @param pZeile
   * @return Wertungen[] alle Wertungen in dieser Zeile.
   */
  private List<IEinzelwertung> getEinzelwertungen(String pZeile) {
	StringTokenizer strTokenizer = new StringTokenizer(pZeile, ";");
	int anzahlToken = strTokenizer.countTokens();
	if (anzahlToken != tokenAnzahl) {
	  throw new IllegalArgumentException("erwarte genau " + tokenAnzahl + " Token. Waren aber " + anzahlToken);
	}
	List<IEinzelwertung> result = new ArrayList<IEinzelwertung>(tokenAnzahl);
	int index = 0;
	while (strTokenizer.hasMoreTokens()) {
	  String token = strTokenizer.nextToken();
	  Wertung wertung = Wertung.transform(token);
	  Einzelwertung ew = new Einzelwertung(getLoesungByIndex(index), wertung);
	  result.add(ew);
	  index++;
	}
	return result;
  }

  private Loesung getLoesungByIndex(int pIndex) {
	String nummer = (pIndex + 1) + "";
	Aufgabenkategorie kategorie = null;
	if (0 <= pIndex && pIndex < 5) {
	  kategorie = Aufgabenkategorie.DREI;
	}
	if (5 <= pIndex && pIndex < 10) {
	  kategorie = Aufgabenkategorie.VIER;
	}
	if (pIndex >= 10) {
	  kategorie = Aufgabenkategorie.FUENF;
	}
	return new Loesung(nummer, kategorie);

  }
}
