/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * @author heike
 */
@XmlRootElement(name = "gesamtpunktverteilung")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "punkte", propOrder = { "titel", "basis", "bewertung", "items", "anzahlTeilnehmer", "median",
	"rohpunktitems", "sectionEinzelergebnisse", "einzelergebnisse" })
public class Gesamtpunktverteilung {

	@XmlElement(name = "titel")
	private String titel;

	@XmlElement(name = "basis")
	private String basis;

	@XmlElement(name = "bewertung")
	private String bewertung;

	@XmlElement(name = "intervallItem")
	private final List<GesamtpunktverteilungItem> items = new ArrayList<GesamtpunktverteilungItem>();

	@XmlElement(name = "teilnehmerzahl")
	private int anzahlTeilnehmer;

	@XmlElement(name = "median")
	private String median;

	@XmlTransient
	private boolean kannGeneriertWerden = false;

	@XmlElement(name = "sectionEinzelergebnisse")
	private String sectionEinzelergebnisse = "Lösungen je Aufgabe";

	@XmlElement(name = "aufgabeErgebnis")
	private List<AufgabeErgebnis> einzelergebnisse;

	@XmlElement(name = "rohpunktitem")
	private List<Rohpunktitem> rohpunktitems;

	/**
	*
	*/
	public Gesamtpunktverteilung() {
		// placeholder
	}

	/**
	 * @return the header
	 */
	public final String getTitel() {
		return titel;
	}

	/**
	 * @param pHeader the header to set
	 */
	public final void setTitel(final String pHeader) {
		titel = pHeader;
	}

	/**
	 * @return the basis
	 */
	public final String getBasis() {
		return basis;
	}

	/**
	 * @param pBasis the basis to set
	 */
	public final void setBasis(final String pBasis) {
		basis = pBasis;
	}

	/**
	 * @return the bewertung
	 */
	public final String getBewertung() {
		return bewertung;
	}

	/**
	 * @param pBewertung the bewertung to set
	 */
	public final void setBewertung(final String pBewertung) {
		bewertung = pBewertung;
	}

	public void addItem(final GesamtpunktverteilungItem pItem) {
		items.add(pItem);
	}

	/**
	 * @return the punktklassen
	 */
	public final List<GesamtpunktverteilungItem> getItemsSorted() {
		Collections.sort(items);
		return Collections.unmodifiableList(items);
	}

	/**
	 * Berechnet anhand aller Items die Prozentränge für die Items.
	 */
	public void berechneProzentraenge() {
		final int gesamt = berechneGesamtzahlTeilnehmer();
		for (final GesamtpunktverteilungItem item : items) {
			anzahlTeilnehmer += item.getAnzahl();
			final int anzahlGleichBesser = anzahlTeilnehmerMitPunktenGleichOderSchlechter(item.getPunktIntervall());
			final double pr = round2(anzahlGleichBesser * 100.0 / gesamt);
			item.setProzentrang(pr+"");
		}
		kannGeneriertWerden = true;
	}

	/**
	 * @param pVal double zu rundende Zahl.
	 * @return double auf 2 dezimalstellen gerundeter Wert von pVal.
	 */
	private double round2(final double pVal) {
		double result = pVal * 100;
		result = Math.round(result);
		result = result / 100;
		return result;
	}

	/**
	 * @return
	 */
	protected int berechneGesamtzahlTeilnehmer() {
		int anzahl = 0;
		for (final GesamtpunktverteilungItem item : items) {
			anzahl += item.getAnzahl();
		}
		return anzahl;
	}

	/**
	 * Ermittelt die Anzhal der Teilnehmer, deren Punkte mindestens im gegebenen Referenzintervall liegen.
	 *
	 * @param pReferenzIntervall
	 * @return int
	 */
	protected int anzahlTeilnehmerMitPunktenGleichOderBesser(final Punktintervall pReferenzIntervall) {
		int anzahl = 0;
		for (final GesamtpunktverteilungItem item : items) {
			final Punktintervall intervall = item.getPunktIntervall();
			if (intervall.compareTo(pReferenzIntervall) <= 0) {
				anzahl += item.getAnzahl();
			}
		}
		return anzahl;
	}

	/**
	 * Ermittelt die Anzhal der Teilnehmer, deren Punkte unterhalb des Referenzintervalls liegen..
	 *
	 * @param pReferenzIntervall
	 * @return int
	 */
	protected int anzahlTeilnehmerMitPunktenGleichOderSchlechter(final Punktintervall pReferenzIntervall) {
		int anzahl = 0;
		for (final GesamtpunktverteilungItem item : items) {
			final Punktintervall intervall = item.getPunktIntervall();
			if (intervall.compareTo(pReferenzIntervall) >= 0) {
				// if (intervall.compareTo(pReferenzIntervall) > 0) {
				anzahl += item.getAnzahl();
			}
		}
		return anzahl;
	}

	/**
	 * @return the kannGeneriertWerden
	 */
	public final boolean isKannGeneriertWerden() {
		return kannGeneriertWerden;
	}

	/**
	 * @return the anzahlTeilnehmer
	 */
	public final int getAnzahlTeilnehmer() {
		return anzahlTeilnehmer;
	}

	/**
	 * @return the einzelergebnisse
	 */
	public List<AufgabeErgebnis> getEinzelergebnisse() {
		return einzelergebnisse;
	}

	/**
	 * @param einzelergebnisse the einzelergebnisse to set
	 */
	public void setEinzelergebnisse(final List<AufgabeErgebnis> einzelergebnisse) {
		this.einzelergebnisse = einzelergebnisse;
	}

	/**
	 * @return the median
	 */
	public String getMedian() {
		return median;
	}

	/**
	 * @param median the median to set
	 */
	public void setMedian(final String median) {
		this.median = median;
	}

	/**
	 * Liefert die Membervariable rohpunktitems
	 *
	 * @return die Membervariable rohpunktitems
	 */
	public List<Rohpunktitem> getRohpunktitems() {
		return rohpunktitems;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param rohpunktitems neuer Wert der Membervariablen rohpunktitems
	 */
	public void setRohpunktitems(final List<Rohpunktitem> rohpunktitems) {
		this.rohpunktitems = rohpunktitems;
	}

	public void setKannGeneriertWerden(final boolean kannGeneriertWerden) {
		this.kannGeneriertWerden = kannGeneriertWerden;
	}
}
