/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

/**
 * Bewertung einer einzelnen Lösung (eine Aufgabe, ein Kind).
 *
 * @author heike
 */
public class Einzelwertung implements IEinzelwertung {

	private Loesung loesung;

	private Wertung wertung;

	/**
	 *
	 */
	public Einzelwertung() {
	}

	/**
	 * @param pAufgabe
	 * @param pWertung
	 */
	public Einzelwertung(Loesung pAufgabe, Wertung pWertung) {
		super();
		loesung = pAufgabe;
		wertung = pWertung;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mcpunktgenerator.domain.IEinzelwertung#getAufgabenkategorie()
	 */
	@Override
	public Aufgabenkategorie getAufgabenkategorie() {
		return loesung.getKategorie();
	}

	/**
	 * @return the aufgabe
	 */
	protected final Loesung getLoesung() {
		return loesung;
	}

	/**
	 * @param pAufgabe the aufgabe to set
	 */
	public final void setLoesung(Loesung pAufgabe) {
		loesung = pAufgabe;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mcpunktgenerator.domain.IEinzelwertung#getWertung()
	 */
	@Override
	public final Wertung getWertung() {
		return wertung;
	}

	/**
	 * @param pWertung the wertung to set
	 */
	public final void setWertung(Wertung pWertung) {
		wertung = pWertung;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((loesung == null) ? 0 : loesung.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Einzelwertung other = (Einzelwertung) obj;
		if (loesung == null) {
			if (other.loesung != null)
				return false;
		} else if (!loesung.equals(other.loesung))
			return false;
		return true;
	}

	/**
	 * Bei Minikänguru gibt es immer 5 Antwortvorschläge.
	 *
	 * @see de.egladil.mcpunktgenerator.domain.IEinzelwertung#getAnzahlAntwortvorschlaege()
	 */
	@Override
	public int getAnzahlAntwortvorschlaege() {
		return 5;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Einzelwertung [loesung=" + loesung + ", wertung=" + wertung + "]";
	}
}
