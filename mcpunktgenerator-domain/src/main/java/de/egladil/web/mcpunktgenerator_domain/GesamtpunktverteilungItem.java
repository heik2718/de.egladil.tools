/**
 *
 */
package de.egladil.web.mcpunktgenerator_domain;

import java.util.StringTokenizer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Ein Eintrag in der Gesamtpunktverteilung. Die Sortierung der Einträge geschieht <b>absteigend</b> nach Punkten!
 *
 * @author heike
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class GesamtpunktverteilungItem implements Comparable<GesamtpunktverteilungItem> {

  @XmlElement(name = "intervall")
  private String punkte;

  @XmlElement(name = "anzahlImIntervall")
  private int anzahl;

  @XmlElement(name = "intervallPR")
  private String prozentrang;

  @XmlTransient
  private Punktintervall punktIntervall;

  /**
   *
   */
  public GesamtpunktverteilungItem() {

  }

  /*
   * (non-Javadoc)
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  @Override
  public int compareTo(final GesamtpunktverteilungItem pO) {
	if (punkte.equals(pO.getPunkte())) {
	  return 0;
	}
	final double minVal = minimum();
	final double paramMinVal = pO.minimum();

	if (minVal == paramMinVal) {
	  return 0;
	}
	if (minVal < paramMinVal) {
	  return 1;
	}
	return -1;
  }

  /**
   * @return the punkte
   */
  public final String getPunkte() {
	return punkte;
  }

  /**
   * @param pPunkte
   *          the punkte to set
   */
  public final void setPunkte(final String pPunkte) {
	punkte = pPunkte;
  }

  /**
   * @return the anzahl
   */
  public final int getAnzahl() {
	return anzahl;
  }

  /**
   * @param pAnzahl
   *          the anzahl to set
   */
  public final void setAnzahl(final int pAnzahl) {
	anzahl = pAnzahl;
  }

//  /**
//   * @return the prozentrang
//   */
//  public final double getProzentrang() {
//	return prozentrang;
//  }
//
//  /**
//   * @param pProzentrang
//   *          the prozentrang to set
//   */
//  public final void setProzentrang(final double pProzentrang) {
//	prozentrang = pProzentrang;
//  }

  /**
   * @return
   */
  private double minimum() {
	final StringTokenizer st = new StringTokenizer(punkte, "-");
	while (st.hasMoreElements()) {
	  final String token = st.nextToken();
	  return Double.valueOf(token);
	}
	return 0.0;
  }

  /**
   * @return
   */
  private double maximum() {
	final StringTokenizer st = new StringTokenizer(punkte, "-");
	int index = 0;
	while (st.hasMoreElements()) {
	  final String token = st.nextToken();
	  if (index == 1) {
		return Double.valueOf(token);
	  }
	  index++;
	}
	return 0.0;
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
	final StringBuilder builder = new StringBuilder();
	builder.append("GesamtpunktverteilungItem [punkte=");
	builder.append(punkte);
	builder.append(", anzahl=");
	builder.append(anzahl);
	builder.append(", prozentrang=");
	builder.append(prozentrang);
	builder.append("]");
	return builder.toString();
  }

  /**
   * Gibt das zu punkte gehörige Punktintervall zurück.
   *
   * @return the punktIntervall
   */
  public final Punktintervall getPunktIntervall() {
	if (punktIntervall == null) {
	  punktIntervall = new Punktintervall(minimum(), maximum());
	}
	return punktIntervall;
  }

public String getProzentrang() {
	return prozentrang;
}

public void setProzentrang(final String prozentrang) {
	this.prozentrang = prozentrang;
}
}
