//=====================================================
// Projekt: de.egladil.tools.mime
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.mime;

/**
 * AcceptableMimeType
 */
public enum AcceptableMimeType {

	VDN_OPEN_XML,
	VDN_MSEXCEL,
	ODS,
	UNKNOWN;
}
