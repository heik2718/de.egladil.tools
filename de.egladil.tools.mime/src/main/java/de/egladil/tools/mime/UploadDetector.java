//=====================================================
// Projekt: de.egladil.tools.mime
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.mime;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.tools.parser.OpenOfficeParser;
import de.egladil.tools.parser.ZipParser;
import de.egladil.tools.parser.exceptions.ParserSecurityException;

/**
 * UploadDetector versucht, den Dateityp eines Uploads mittels Heuristik zu erraten. Dabei wird on einer Teilmenge von
 * zu erwartenden Dateitypen ausgegangen.
 */
public class UploadDetector {

	private static final String UTF_8 = "utf-8";

	private static final Logger LOG = LoggerFactory.getLogger(UploadDetector.class);

	private static final String MIMETYPE_OPEN_OFFICE = "application/vnd.oasis.opendocument.spreadsheet";

	/**
	 * Returns the mime-type of the given byte-Array.
	 *
	 * @param bytes byte[]
	 *
	 * @return String
	 * @throws IOException
	 */
	public String detect(final byte[] bytes) throws ParserSecurityException, IOException {
		if (bytes == null) {
			throw new IllegalArgumentException("bytes null");
		}

		AcceptableMimeType mimeType = testMsExcel(bytes);
		if (mimeType != null) {
			return mimeType.toString();
		}

		mimeType = testXlsx(bytes);
		if (mimeType != null) {
			return mimeType.toString();
		}

		mimeType = testOpenOffice(bytes);
		if (mimeType != null) {
			return mimeType.toString();
		}

		return AcceptableMimeType.UNKNOWN.toString();
	}

	private AcceptableMimeType testMsExcel(final byte[] bytes) throws IOException {
		try (HSSFWorkbook wb = new HSSFWorkbook(new ByteArrayInputStream(bytes))) {
			return AcceptableMimeType.VDN_MSEXCEL;
		} catch (final Exception e) {
			LOG.warn("Excel-97-Test (xls) schlug fehl: " + e.getMessage());
			return null;
		}
	}

	private AcceptableMimeType testXlsx(final byte[] bytes) {
		try (XSSFWorkbook wb = new XSSFWorkbook(new ByteArrayInputStream(bytes))) {
			return AcceptableMimeType.VDN_OPEN_XML;
		} catch (final Exception e) {
			LOG.warn("Excel-2003-Test (xlsx) schlug fehl: " + e.getMessage());
			return null;
		}
	}

	private AcceptableMimeType testOpenOffice(final byte[] bytes) throws ParserSecurityException, IOException {
		final ZipParser zipParser = new ZipParser();

		try {
			final String mimeType = zipParser.getContentOfEntrySafe(bytes, OpenOfficeParser.ENTRY_NAME_MIMETYPE, 200, UTF_8);
			if (MIMETYPE_OPEN_OFFICE.equals(mimeType)) {
				return AcceptableMimeType.ODS;
			}
		} catch (final Exception e) {
			LOG.warn("OpenOffice-Test (ods) schlug fehl: " + e.getMessage());
			return null;
		}
		return null;
	}

}
