//=====================================================
// Projekt: de.egladil.tools.mime
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.mime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import de.egladil.tools.parser.exceptions.ParserSecurityException;

/**
 * UploadDetectorTest
 */
public class UploadDetectorTest {

	@Test
	void detectXlsxKlappt() {

		// Arrange
		final UploadDetector detector = new UploadDetector();

		try (InputStream in = getClass().getResourceAsStream("/auswertung.xlsx")) {
			final byte[] bytes = IOUtils.toByteArray(in);
			final String actual = detector.detect(bytes);
			assertEquals(AcceptableMimeType.VDN_OPEN_XML.toString(), actual);
		} catch (final IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void detectXlsKlappt() {

		// Arrange
		final UploadDetector detector = new UploadDetector();

		try (InputStream in = getClass().getResourceAsStream("/auswertung.xls")) {
			final byte[] bytes = IOUtils.toByteArray(in);
			final String actual = detector.detect(bytes);
			assertEquals(AcceptableMimeType.VDN_MSEXCEL.toString(), actual);
		} catch (final IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void detectOpenOfficeKlappt() {

		// Arrange
		final UploadDetector detector = new UploadDetector();

		try (InputStream in = getClass().getResourceAsStream("/auswertung.ods")) {
			final byte[] bytes = IOUtils.toByteArray(in);
			final String actual = detector.detect(bytes);
			assertEquals(AcceptableMimeType.ODS.toString(), actual);
		} catch (final IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void detectFakeOpenOfficeMitZipBombe() {

		// Arrange
		final UploadDetector detector = new UploadDetector();

		try (InputStream in = getClass().getResourceAsStream("/zipBomb-201.txt")) {
			final byte[] bytes = IOUtils.toByteArray(in);
			detector.detect(bytes);
			fail("keine ParserSecurityException");
		} catch (final IOException e) {
			fail(e.getMessage());
		} catch (final ParserSecurityException e) {
			assertEquals("ZipEntry 'mimetype' ist größer als 200 Bytes mit kleinem Puffer", e.getMessage());
		}
	}

	@Test
	void detectVulnerableXlsx() {
		// Arrange
		final UploadDetector detector = new UploadDetector();

		try (InputStream in = getClass().getResourceAsStream("/vulnerable.xlsx")) {
			final byte[] bytes = IOUtils.toByteArray(in);
			detector.detect(bytes);
			fail("keine ParserSecurityException");
		} catch (final IOException e) {
			fail(e.getMessage());
		} catch (final ParserSecurityException e) {
			assertEquals("Das Zip-Archiv enthält keinen ZipEntry mit Namen 'mimetype' in oberster Ebene.", e.getMessage());
		}
	}
}
