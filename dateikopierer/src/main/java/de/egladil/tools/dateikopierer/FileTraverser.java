//=====================================================
// Projekt: dateikopierer
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.dateikopierer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * FileTraverser
 */
public class FileTraverser {

	private final List<String> paths;

	/**
	 * FileTraverser
	 */
	public FileTraverser() {
		paths = new ArrayList<>();
	}

	/**
	 * Traversiert den Teilbaum rekursiv deep first und sammelt die echten Dateien in der Reihenfolge.
	 *
	 * @param sourcePath String
	 * @param extension String die Dateinamenerweiterung, die alle gefundenen Dateien haben müssen.
	 * @return List
	 */
	public FileTraverser getFilesDeepFirst(final String sourcePath, final String extension) {
		String extensionWithPoint = null;
		if (extension != null) {
			if (extension.contains(".")) {
				extensionWithPoint = extension;
			} else {
				extensionWithPoint = "." + extension;
			}
		}

		readDir(sourcePath, extensionWithPoint);
		return this;
	}

	private void readDir(final String pathName, final String extensionWithPoint) {
		final File file = new File(pathName);
		if (file.isDirectory()) {
			final List<String> dateien = new DateinamenFinder().getDateinamenSorted(file, extensionWithPoint);
			paths.addAll(dateien);
			final File[] files = file.listFiles();
			if (files != null) {
				for (final File f : files) {
					readDir(f.getAbsolutePath(), extensionWithPoint);
				}
			}
		}
	}

	public final List<String> getPaths() {
		return paths;
	}
}
