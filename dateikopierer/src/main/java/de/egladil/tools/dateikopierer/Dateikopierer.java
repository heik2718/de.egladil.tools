//=====================================================
// Projekt: dateikopierer
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.dateikopierer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dateikopierer
 */
public class Dateikopierer {

	private static final Logger LOG = LoggerFactory.getLogger(Dateikopierer.class);

	private final String sourcePath;

	private final String targetPath;

	private final String fileExtensionMitPunkt;

	private int offset;

	/**
	 * Dateikopierer
	 */
	public Dateikopierer(final String sourcePath, final String targetPath, final String fileExtensionMitPunkt, final int offset) {
		this.sourcePath = sourcePath;
		this.targetPath = targetPath;
		this.fileExtensionMitPunkt = fileExtensionMitPunkt;
		this.offset = offset;

	}

	public void copyAndRename() {

		final List<String> filePaths = new FileTraverser().getFilesDeepFirst(sourcePath, fileExtensionMitPunkt).getPaths();

		final int anzGesamt = filePaths.size();
		int anzAktuell = 0;
		LOG.info("kopieren {} Dateien sequentiell", anzGesamt);

		for (final String path : filePaths) {
			final File sourceFile = new File(path);
			final String targetFilename = StringUtils.leftPad("" + offset, 4, '0') + fileExtensionMitPunkt;
			final File targetFile = new File(targetPath + File.separator + targetFilename);
			try (FileInputStream in = new FileInputStream(sourceFile); FileOutputStream out = new FileOutputStream(targetFile)) {
				IOUtils.copy(in, out);
				out.flush();
				anzAktuell++;
				offset++;
				LOG.info("{} von {} kopiert - Pfad '{}'", anzAktuell, anzGesamt, sourceFile.getAbsolutePath());
			} catch (final IOException e) {
				throw new RuntimeException("kann Datei " + sourceFile.getName() + " nicht kopieren: " + e.getMessage(), e);
			}
		}
		LOG.info("fertisch :)");
	}
}
