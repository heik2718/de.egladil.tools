//=====================================================
// Projekt: dateikopierer
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.dateikopierer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * DateinamenFinder
 */
public class DateinamenFinder {

	public List<String> getDateinamenSorted(final File verzeichnis, final String fileExtensionWithPoint) {
		final List<String> result = new ArrayList<>();
		final File[] files = verzeichnis.listFiles();
		for (final File f : files) {
			if (f.getName().endsWith(fileExtensionWithPoint)) {
				result.add(f.getAbsolutePath());
			}
		}
		Collections.sort(result);
		return result;
	}

}
