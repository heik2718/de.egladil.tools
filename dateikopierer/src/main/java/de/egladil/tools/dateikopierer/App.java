package de.egladil.tools.dateikopierer;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

/**
 * Hello world!
 *
 */
public class App {

	private static final Logger LOG = LoggerFactory.getLogger(App.class);

	@Parameter(names = { "-s" }, description = "voller Pfad zum Source-Verzeichnis", required = true)
	private String sourceDir;

	@Parameter(names = { "-t" }, description = "voller Pfad zum Source-Verzeichnis", required = true)
	private String targetDir;

	@Parameter(names = { "-e" }, description = "Dateierweiterung für die Arten der zu kopierenden  Dateien. default ist mp3")
	private String fileExtension;

	@Parameter(names = { "-o" }, description = "offset für den Dateinamenzähler. Default ist 0")
	private int filenameOffset;

	public static void main(final String[] args) {
		App application = null;
		try {
			application = new App();
			new JCommander(application, args);
			LOG.info(application.toString());
			application.start();
			System.exit(0);
		} catch (final ParameterException e) {
			System.err.println(e.getMessage());
			if (application != null) {
				application.printUsage();
			}
			System.exit(1);
		} catch (final Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	private void printUsage() {
		final StringBuffer sb = new StringBuffer();
		sb.append("Usage: java -jar dateikopierer.jar [options]\n");
		sb.append("   Options:\n");
		sb.append("     * -s\n");
		sb.append("          voller Pfad zum Source-Verzeichnis\n");
		sb.append("     * -t\n");
		sb.append("          voller Pfad zum Source-Verzeichnis\n");
		sb.append("       -e\n");
		sb.append("          Dateierweiterung für die Arten der zu kopierenden  Dateien. default ist mp3\n");
		sb.append("       -o\n");
		sb.append("          startnummer fuer den Dateinamen. default is 0\n");
		System.out.println(sb.toString());
	}

	private void start() {
		final File source = new File(sourceDir);
		final File target = new File(targetDir);
		if (!source.isDirectory() || !source.canRead()) {
			throw new RuntimeException("kein Zugriff auf Verzeichnis " + sourceDir);
		}
		if (!target.isDirectory() || !target.canWrite()) {
			throw new RuntimeException("kein Zugriff auf Verzeichnis " + targetDir);
		}
		String fileExtensionWithPoint = ".mp3";
		if (fileExtension != null) {
			fileExtensionWithPoint = fileExtension.contains(".") ? fileExtension : "." + fileExtension;
		}
		final Dateikopierer kopierer = new Dateikopierer(sourceDir, targetDir, fileExtensionWithPoint, filenameOffset);
		kopierer.copyAndRename();
	}
}
