//=====================================================
// Projekt: dateikopierer
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.dateikopierer;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * FileTraverserTest
 */
public class FileTraverserTest {

	@Test
	void traverseDeepFirst() {
		// Arrange
		final String sourcePath = "/home/heike/Musik/Richard Dübell - Die Wächter der Teufelsbibel";
		final String extension = "mp3";

		// Act
		final File file = new File(sourcePath);
		final List<String> files = new FileTraverser().getFilesDeepFirst(sourcePath, extension).getPaths();

		// Assert
		assertTrue(file.isDirectory());
		assertEquals(76, files.size());

		for (final String f : files) {
			System.out.println(f);
		}
	}
}
