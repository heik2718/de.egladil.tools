//=====================================================
// Projekt: dateikopierer
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.dateikopierer;

import java.io.File;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * DateikopiererTest
 */
@Disabled
public class DateikopiererTest {

	@Test
	@DisplayName("test mit offset 0")
	void happyHourOffset0() {
		final File sourceDir = new File("/home/heike/Musik/follett/source");
		final File targetDir = new File("/home/heike/Musik/follett/target");
		new Dateikopierer(sourceDir.getAbsolutePath(), targetDir.getAbsolutePath(), ".mp3", 0).copyAndRename();
	}

//	@Test
	@DisplayName("test mit offset 5")
	void happyHourOffset5() {
		final File sourceDir = new File("/home/heike/Music/sourcedir");
		final File targetDir = new File("/home/heike/Music/targetdir");
		new Dateikopierer(sourceDir.getAbsolutePath(), targetDir.getAbsolutePath(), ".mp3", 5).copyAndRename();
	}
}
