/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;



/**
 * Wandelt eine einzelne CSV-Datei in die ;-separierten Strings um.
 *
 * @author heike
 */
public class SingleCSVReader {

  /**
   *
   */
  public SingleCSVReader() {
  }

  /**
   * Wandelt eine einzelne CSV-Datei in die ;-separierten Strings um.
   *
   * @param pFile
   *          File eine CSV-Datei.
   * @return List die Zeilen.
   */
  public List<String> processResource(File pFile) {
	if (pFile == null) {
	  throw new IllegalArgumentException("Der Parameter darf nicht null sein.");
	}
	if (!pFile.exists() || !pFile.canRead()) {
	  String msg = "Die datei [" + pFile.getAbsolutePath() + "] existiert nicht oder ist nicht lesbar.";
	  System.err.println(msg);
	  throw new IllegalArgumentException(msg);
	}
	FileReader fr = null;
	try {
	  fr = new FileReader(pFile);
	  return IOUtils.readLines(fr);
	} catch (Exception e) {
	  final String msg = "Fehler beim Lesen [file=" + pFile.getAbsolutePath() + "]: " + e.getMessage();
	  System.err.println(msg);
	  throw new RuntimeException(msg, e);
	} finally {
	  IOUtils.closeQuietly(fr);
	}
  }

  /**
   * Liest die Daten aus einem InputStream. Dies ist eine convenience-Methode für Testzwecke. <br>
   * <br>
   * Achtung: der InputStream wird nicht geschlossen.
   *
   * @param pIn
   *          InputStream dieser darf nicht null sein.
   * @return List die Zeilen
   */
  public List<String> processResource(InputStream pIn) {
	if (pIn == null){
	  throw new IllegalArgumentException("pIn darf nicht null sein");
	}
	File tempFile = null;
	OutputStream out = null;
	try {
	  tempFile = File.createTempFile("temp", ".csv");
	  tempFile.deleteOnExit();
	  out = new FileOutputStream(tempFile);
	  IOUtils.copy(pIn, out);
	  out.flush();
	  return processResource(tempFile);
	} catch (IOException e) {
	  throw new RuntimeException("konnte InputStream nicht lesen" + e.getMessage(), e);
	}
  }
}
