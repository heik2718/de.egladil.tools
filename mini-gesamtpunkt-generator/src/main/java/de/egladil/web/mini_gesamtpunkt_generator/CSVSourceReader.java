package de.egladil.web.mini_gesamtpunkt_generator;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * @author Winkelv
 */
public class CSVSourceReader {

  private final String path;

  /**
   * @param pPath
   */
  public CSVSourceReader(String pPath) {
	super();
	path = pPath;
  }

  /**
   *
   */
  public List<File> listFiles() {
	File dir = new File(path);
	if (!dir.isDirectory() || !dir.canRead()) {
	  final String msg = path + " ist kein gueltiges Verzeichnis";
	  System.err.println(msg);
	  throw new RuntimeException(msg);
	}
	File[] files = dir.listFiles(new FilenameFilter() {
	  public boolean accept(File pDir, String pName) {
		return (pName.endsWith(".csv"));
	  }
	});
	System.out.println(files.length + " Dateien gefunden");
	return Collections.unmodifiableList(Arrays.asList(files));
  }
}
