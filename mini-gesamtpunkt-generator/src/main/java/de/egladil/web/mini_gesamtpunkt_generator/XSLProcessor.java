/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 * @author heike
 */
public class XSLProcessor {

	private static final String DEFAULT_ENCODING = "UTF-8";

	class MyErrorListener implements ErrorListener {
		private static final String TYPE_WARN = "Warning";

		private static final String TYPE_ERROR = "Error";

		private static final String TYPE_FATAL = "Fatal Error";

		public void warning(TransformerException e) throws TransformerException {
			show(TYPE_WARN, e);
			throw (e);
		}

		public void error(TransformerException e) throws TransformerException {
			show(TYPE_ERROR, e);
			throw (e);
		}

		public void fatalError(TransformerException e) throws TransformerException {
			show(TYPE_FATAL, e);
			throw (e);
		}

		private void show(String type, TransformerException e) {
			if (TYPE_WARN.equals(type)) {
				System.err.println(type + ": " + e.getMessage());
				if (e.getLocationAsString() != null) {
					System.err.println(type + ": " + e.getMessage());
				}
			} else {
				System.err.println(type + ": " + e.getMessage());
				if (e.getLocationAsString() != null) {
					System.err.println(type + ": " + e.getMessage());
				}
			}
		}
	}

	/**
   *
   */
	public XSLProcessor() {
	}

	/**
	 * Wandelt das Objekt in xml um und transformiert es mit der xsl-Resource, sofern diese nicht null ist. pXslResource
	 * wird nicht geschlossen. Es werden keine Verzeichnisse angelegt. Das muss der Aufrufer tun.
	 *
	 * @param pJaxbContext
	 * @param pObject
	 * @param pXslResource InputStream falls null, dann wird keine XSL-Transformation angeschlossen, sondern nur xml
	 * generiert.
	 * @param pPathOutput voll qualifizierter Pfad für die Datei, die durch diesen Prozess erzeugt werden soll.
	 * @deprecated
	 */
	protected void marshallAndProcess(JAXBContext pJaxbContext, Object pObject, InputStream pXslResource, String pPathOutput) {
		if (pJaxbContext == null) {
			throw new IllegalArgumentException("pJaxbContext darf nicht null sein");
		}
		if (pObject == null) {
			throw new IllegalArgumentException("pObject darf nicht null sein");
		}
		if (pPathOutput == null || pPathOutput.isEmpty()) {
			throw new IllegalArgumentException("pPathOutput darf nicht null der leer sein");
		}

		OutputStream out = null;
		try {
			File xmlTempFile = File.createTempFile("gesamtpunktverteilung", "xml");
			xmlTempFile.deleteOnExit();

			final Marshaller marshaller = pJaxbContext.createMarshaller();
			marshaller.setProperty("jaxb.encoding", DEFAULT_ENCODING);
			marshaller.marshal(pObject, xmlTempFile);
			if (pXslResource != null) {
				File xslTempFile = File.createTempFile("transformation", "xslt");
				xslTempFile.deleteOnExit();
				out = new FileOutputStream(xslTempFile);
				IOUtils.copy(pXslResource, out);
				out.flush();
				transform(xmlTempFile.getAbsolutePath(), xslTempFile.getAbsolutePath(), pPathOutput, null, null);
				System.out.println("Info: mit xslt: Ausgabe nach [" + pPathOutput + "]");
			} else {
				FileUtils.copyFile(xmlTempFile, new File(pPathOutput));
				System.out.println("Info: ohne xslt: Ausgabe nach [" + pPathOutput + "]");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Marshalling oder XSL-Transformation fehlgeschlagen.", e);
		} finally {
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * Führt eine XSL-Transformation durch.
	 *
	 * @param pathInXml absoluter Pfad zu einer XML-Datei (darf nicht null sein)
	 * @param pathInXsl absoluter Pfad zu einer XSL-Datei (darf nicht null sein)
	 * @param pathOut absoluter Pfad der Ausgabedatei (darf nicht null sein)
	 * @param pEncoding String das Encoding. Bei null wird als default ISO-8859-1 verwendet.
	 * @param pParameters Map Parameter, die an das stylesheet übergeben werden sollen (encoding muss nicht als
	 * Parameter übergeben werden). Darf null sein.
	 */
	public void transform(String pathInXml, String pathInXsl, String pathOut, String pEncoding, Map<String, String> pParameters) {
		if (pathInXml == null) {
			throw new IllegalArgumentException("pathInXml darf nicht null sein!");
		}
		if (pathOut == null) {
			throw new IllegalArgumentException("pathOut darf nicht null sein!");
		}
		if (pathInXsl == null) {
			throw new IllegalArgumentException("pathInXsl darf nicht null sein!");
		}
		try {
			TransformerFactory factory = TransformerFactory.newInstance();

			StreamSource xslStream = new StreamSource(pathInXsl);
			Transformer transformer = factory.newTransformer(xslStream);
			transformer.setOutputProperty(OutputKeys.METHOD, "text");
			transformer.setErrorListener(new MyErrorListener());
			final String encoding = pEncoding != null ? pEncoding : DEFAULT_ENCODING;
			transformer.setOutputProperty(OutputKeys.ENCODING, encoding);
			transformer.setParameter(OutputKeys.ENCODING, encoding);
			if (pParameters != null) {
				for (String key : pParameters.keySet()) {
					String value = pParameters.get(key);
					if (value != null) {
						transformer.setParameter(key, value);
					}
				}
			}

			StreamSource in = new StreamSource(pathInXml);
			StreamResult out = new StreamResult(pathOut);
			transformer.transform(in, out);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("XSL-Transformation fehlgeschlagen.", e);
		}

	}
}
