/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.egladil.web.mcpunktgenerator_domain.Loesungszettel;
import de.egladil.web.mcpunktgenerator_domain.Zeilenumwandler;

/**
 * Das ist die Klasse, die ein Verzeichnis mit csv-Dateien durchnudelt und diese in Loesungszettel umwandelt.
 *
 * @author heike
 */
public class LoesungszettelGenerator {

	private final SingleCSVReader csvReader;

	private final Zeilenumwandler zeilenumwandler;

	private int anzahlSchulen;

	/**
   *
   */
	public LoesungszettelGenerator() {
		csvReader = new SingleCSVReader();
		zeilenumwandler = new Zeilenumwandler();
	}

	/**
	 * Geht alle csv-Dateien in dem gegebenen Verzeichnis durch und sammelt die Punkte. Anschließend kann die Anzahl der
	 * Schulen abgefragt werden.
	 *
	 * @param pDir
	 * @return List
	 */
	public List<Loesungszettel> processDir(File pDir) {
		List<File> files = new CSVSourceReader(pDir.getAbsolutePath()).listFiles();
		int fileCount = 0;
		List<Loesungszettel> result = new ArrayList<Loesungszettel>();
		for (File file : files) {
			System.out.println("verarbeite " + file.getName() + " - Nummer " + ++fileCount + " von " + files.size());
			List<String> lines = csvReader.processResource(file);
			int count = 0;
			for (String line : lines) {
				count++;
				try {
					Loesungszettel zettel = zeilenumwandler.umwandeln(line);
					result.add(zettel);
				} catch (Exception e) {
					System.err.println("Fehler bei [file=" + file.getName() + ", Zeile=" + count + "]: " + e.getMessage());
					throw new RuntimeException(e.getMessage(), e);
				}
			}
		}
		anzahlSchulen = files.size();
		System.out.println("[Anzahl Dateien=" + files.size() + ", Anzahl Loesungszettel=" + result.size() + "]");
		return result;
	}

	/**
	 * @return the anzahlSchulen
	 */
	public final int getAnzahlSchulen() {
		return anzahlSchulen;
	}
}
