package de.egladil.web.mini_gesamtpunkt_generator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.egladil.web.mcpunktgenerator_domain.AufgabeErgebnis;
import de.egladil.web.mcpunktgenerator_domain.IEinzelwertung;
import de.egladil.web.mcpunktgenerator_domain.Loesungszettel;
import de.egladil.web.mcpunktgenerator_domain.Rohpunktitem;

/**
 *
 * @author heike
 *
 */
public class StatistikUtils {

	private Comparator<Rohpunktitem> rohpunktitemPunkteComparatorAsc = new Comparator<Rohpunktitem>() {

		@Override
		public int compare(final Rohpunktitem o1, final Rohpunktitem o2) {
			final int value = Double.valueOf(o1.getPunkte()).compareTo(Double.valueOf(o2.getPunkte()));
			return value;
		}

	};

	private Comparator<Rohpunktitem> rohpunktitemPunkteComparatorDesc = new Comparator<Rohpunktitem>() {

		@Override
		public int compare(final Rohpunktitem o1, final Rohpunktitem o2) {
			final int value = Double.valueOf(o2.getPunkte()).compareTo(Double.valueOf(o1.getPunkte()));
			return value;
		}

	};

	/**
	 *
	 * @param alleLoesungszettel
	 * @return
	 */
	public double berechneArithmetischesMittel(final List<Loesungszettel> alleLoesungszettel) {
		double result = 0.0;
		double summe = 0.0;
		for (final Loesungszettel zettel : alleLoesungszettel) {
			summe += zettel.berechneGesamtpunktzahl();
		}
		result = summe / alleLoesungszettel.size();
		return result;
	}

	/**
	 *
	 * @param loesungszettel
	 * @return
	 */
	public double berechneMedian(final List<Loesungszettel> loesungszettel) {
		final List<Double> punkte = getMesswerte(loesungszettel);
		Collections.sort(punkte);
		System.out.println("kleinster = " + punkte.get(0));
		System.out.println("groesster = " + punkte.get(punkte.size() - 1));

		final double anzahl = punkte.size() + 0.0;
		final double haelfte = anzahl / 2;

		double result = 0.0;

		if (Math.floor(haelfte) < (punkte.size() + 0.0) / 2) {
			final double erstes = punkte.get(punkte.size() / 2 - 1);
			final double zweites = punkte.get(punkte.size() / 2);

			System.out.println("erstes=" + erstes + ", zweites=" + zweites);
			result = (erstes + zweites) / 2;
		} else {
			final int index = punkte.size() / 2 - 1;
			result = punkte.get(index);
		}

		return result;
	}

	/**
	 * Erzeugt eine Liste von allen vorhandenen Meßwerten.
	 *
	 * @param loesungszettel
	 * @return
	 */
	List<Double> getMesswerte(final List<Loesungszettel> loesungszettel) {
		final List<Double> punkte = new ArrayList<Double>();
		for (final Loesungszettel z : loesungszettel) {
			punkte.add(z.berechneGesamtpunktzahl());
		}
		return punkte;
	}

	/**
	 *
	 * TODO
	 *
	 * @param itemsSortiert
	 */
	public List<Rohpunktitem> berechneProzentraenge(final List<Rohpunktitem> items) {
		// zum Kumulieren aufsteigend nach Punkten
		items.sort(rohpunktitemPunkteComparatorAsc);

		int kumuliert = 0;
		int gesamt = 0;
		for (final Rohpunktitem item : items) {
			kumuliert += item.getAnzahl();
			item.setAnzahlKumuliert(kumuliert);
			gesamt += item.getAnzahl();
		}
		for (final Rohpunktitem item : items) {
			final double prozentrang = item.getAnzahlKumuliert() * 100.0 / gesamt;
			item.setProzentrang(prozentrang);
		}

		// zur Ausgabe absteigend nach Punkten
		items.sort(rohpunktitemPunkteComparatorDesc);
		return items;
	}

	/**
	 *
	 * @param loesungszettel
	 * @param index
	 * @return
	 */
	public AufgabeErgebnis berechneAufgabeErgebnis(final List<Loesungszettel> loesungszettel, final int index) {
		int anzR = 0;
		int anzN = 0;
		int anzF = 0;
		for (final Loesungszettel z : loesungszettel) {
			final IEinzelwertung wertung = z.getEinzelwertungen().get(index);
			switch (wertung.getWertung()) {
			case RICHTIG:
				anzR++;
				break;
			case LEER:
				anzN++;
				break;
			case FALSCH:
				anzF++;
			default:
				break;
			}
		}
		final AufgabeErgebnis ergebnis = new AufgabeErgebnis(index, anzR, anzN, anzF);

		final int anzahlLoesungszettel = loesungszettel.size();

		final double antR = Double.valueOf("" + anzR) / anzahlLoesungszettel * 100;
		final double antN = Double.valueOf("" + anzN) / anzahlLoesungszettel * 100;
		final double antF = Double.valueOf("" + anzF) / anzahlLoesungszettel * 100;

		ergebnis.setAnteilRichtigGeloest(round(antR, 2) + "");
		ergebnis.setAnteilNichtGeloest(round(antN, 2) + "");
		ergebnis.setAnteilFalschGeloest(round(antF, 2) + "");
		return ergebnis;
	}

	private double round(final double value, final int anzDecimal) {
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(anzDecimal, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
