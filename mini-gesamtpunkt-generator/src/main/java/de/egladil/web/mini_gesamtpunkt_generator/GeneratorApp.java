package de.egladil.web.mini_gesamtpunkt_generator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

/**
 *
 */
public class GeneratorApp {

	private static final Logger LOG = LoggerFactory.getLogger(GeneratorApp.class);

	private static final String KEY_DATEINAME = "dateiname";

	@Parameter(names = {
		"--source" }, description = "absoluter Pfad zum Verzeichnis mit den CSV-Dateien, die generiert werden sollen", required = true)
	private String sourceDir;

	@Parameter(names = {
		"--target" }, description = "absoluter Pfad zum Verzeichnis mit der Ausgabedatei", required = true)
	private String targetDir;

	@Parameter(names = { "-j" }, description = "Jahr 4stellig", required = true)
	private String jahr;

	@Parameter(names = { "-k" }, description = "Klasse 1stellig", required = true)
	private int klasse;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		GeneratorApp application = null;
		try {
			application = new GeneratorApp();
			new JCommander(application, args);
			LOG.info(application.toString());
			application.run();
		} catch (ParameterException e) {
			System.err.println(e.getMessage());
			if (application != null) {
				application.printUsage();
			}
			System.exit(1);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	private void run() {
		String targetPath = targetDir
			+ TexteProperties.getInstance().getPropertyFormated(KEY_DATEINAME, new Object[] { jahr, klasse });
		Generator generator = new Generator(targetPath);
		generator.run(sourceDir, jahr, klasse + "");

	}

	/**
	*
	*/
	private static void printUsage() {
		StringBuffer sb = new StringBuffer();
		sb.append("Usage: <main class> [options]\n");
		sb.append("   Options:\n");
		sb.append("     * --source\n");
		sb.append("          absoluter Pfad zum Verzeichnis mit den CSV-Dateien, die generiert werden sollen\n");
		sb.append("     * --target\n");
		sb.append("          absoluter Pfad der Ausgabedatei\n");
		sb.append("     * -j\n");
		sb.append("          Jahr 4stellig\n");
		sb.append("     * -k\n");
		sb.append("          Klasse 1stellig\n");
		sb.append("\n");
		sb.append(
			"\nBeispiel:\n  java -jar mini-gesamtpunkt-generator.jar --source /home/heike/.minikaengGenerator/2014/csv-klasse1 --target /home/heike/work/knobelarchiv_2/latex/ -j 2014 -k 1");
		System.out.println(sb.toString());
		//2016/klasse2 20160622_punktverteilung_klasse2.tex 2016 2
		//--source /home/heike/.minikaengGenerator/2016/klasse1 --target /home/heike/work/knobelarchiv_2/latex/ -j 2016 -k 1
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GeneratorApp [sourceDir=" + sourceDir + ", targetDir=" + targetDir + ", jahr=" + jahr + ", klasse="
			+ klasse + "]";
	}
}
