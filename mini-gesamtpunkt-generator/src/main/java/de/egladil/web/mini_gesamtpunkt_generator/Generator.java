/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.IOUtils;

import de.egladil.web.mcpunktgenerator_domain.Gesamtpunktverteilung;
import de.egladil.web.mcpunktgenerator_domain.Loesungszettel;

/**
 * Diese Klasse generiert das xml aus den im gegebenen Verzeichnis liegenden CSV-Dateien.
 *
 * @author heike
 */
public class Generator {

	private final LoesungszettelGenerator loesungszettelGenerator;

	private final GesamtverteilungGenerator gesamtverteilungGenerator;

	private final JAXBContext jaxbContext;

	private final String targetPath;

	/**
	 * @param targetPath String absoluter Pfad zur LaTeX-Ausgabedatei.
	 * @throws EgladilBaseException falls der jaxbContext nicht erzeugt werden kann.
	 */
	public Generator(final String targetPath) {
		loesungszettelGenerator = new LoesungszettelGenerator();
		gesamtverteilungGenerator = new GesamtverteilungGenerator();
		this.targetPath = targetPath;
		try {
			jaxbContext = JAXBContext.newInstance(Gesamtpunktverteilung.class);
		} catch (final JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException("JAXBContext konnte nicht erstellt werden: " + e.getMessage(), e);
		}
	}

	/**
	 * @param pCSVDirPath
	 * @param jahr
	 */
	public void run(final String pCSVDirPath, final String jahr, final String klasse) throws RuntimeException {
		final Gesamtpunktverteilung verteilung = createVerteilung(pCSVDirPath, jahr, klasse);
		createOutput(verteilung);
	}

	/**
	 * @param verteilung
	 */
	void createOutput(final Gesamtpunktverteilung verteilung) {
		if (!verteilung.isKannGeneriertWerden()) {
			throw new RuntimeException("Fehler im Ablauf: die Prozentraenge wurden noch nicht berechnet");
		}

		InputStream in = null;
		try {
			in = getClass().getResourceAsStream("/gesamtpunktverteilungLaTeXGenerator.xsl");
			marshallAndTransform(verteilung, in, targetPath);
			System.out.println("Gesamtpunktverteilung generiert [path=" + targetPath + "]");
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	/**
	 * Wandelt das Objekt in xml um und transformiert es mit der xsl-Resource, sofern diese nicht null ist. pXslResource
	 * wird nicht geschlossen. Es werden keine Verzeichnisse angelegt. Das muss der Aufrufer tun.
	 *
	 * @param pXslResource InputStream falls null, dann wird keine XSL-Transformation angeschlossen, sondern nur xml
	 * generiert.
	 * @param pPathOutput voll qualifizierter Pfad für die Datei, die durch diesen Prozess erzeugt werden soll.
	 */
	private void marshallAndTransform(final Gesamtpunktverteilung pVerteilung, final InputStream pXslResource, final String pPathOutput) {
		if (pPathOutput == null || pPathOutput.isEmpty()) {
			throw new RuntimeException("pPathOutput darf nicht null oder leer sein.");
		}

		OutputStream out = null;
		try {
			final File xmlTempFile = File.createTempFile("gesamtpunktverteilung", "xml");
			// System.err.println("Pfad zum XML=" + xmlTempFile.getAbsolutePath());
			xmlTempFile.deleteOnExit();

			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty("jaxb.encoding", "UTF-8");
			marshaller.marshal(pVerteilung, xmlTempFile);
			final File xslTempFile = File.createTempFile("transformation", "xslt");
			xslTempFile.deleteOnExit();
			out = new FileOutputStream(xslTempFile);
			IOUtils.copy(pXslResource, out);
			out.flush();
			final XSLProcessor processor = new XSLProcessor();
			processor.transform(xmlTempFile.getAbsolutePath(), xslTempFile.getAbsolutePath(), pPathOutput, null, null);
			System.out.println("mit xslt: Ausgabe nach [" + pPathOutput + "]");
		} catch (final Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Marshalling oder XSL-Transformation fehlgeschlagen.", e);
		} finally {
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * @param pCSVDirPath
	 * @param klasse
	 * @param jahr
	 * @return
	 */
	protected Gesamtpunktverteilung createVerteilung(final String pCSVDirPath, final String jahr, final String klasse) {
		final List<Loesungszettel> loesungszettel = loesungszettelGenerator.processDir(new File(pCSVDirPath));
		final Gesamtpunktverteilung verteilung = gesamtverteilungGenerator.create(loesungszettel, jahr, klasse);
		verteilung.setBasis(TexteProperties.getInstance().getPropertyFormated("grundlage.description",
			new Object[] { loesungszettel.size() }));
		verteilung.berechneProzentraenge();
		return verteilung;
	}
}
