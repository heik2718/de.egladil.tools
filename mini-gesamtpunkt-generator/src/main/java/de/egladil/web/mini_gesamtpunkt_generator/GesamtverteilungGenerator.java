/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.egladil.web.mcpunktgenerator_domain.AufgabeErgebnis;
import de.egladil.web.mcpunktgenerator_domain.Gesamtpunktverteilung;
import de.egladil.web.mcpunktgenerator_domain.GesamtpunktverteilungItem;
import de.egladil.web.mcpunktgenerator_domain.Loesungszettel;
import de.egladil.web.mcpunktgenerator_domain.Punktintervall;
import de.egladil.web.mcpunktgenerator_domain.Punktintervallklasse;
import de.egladil.web.mcpunktgenerator_domain.Rohpunktitem;

/**
 * Das ist die Klasse, die die csv-Dateien durchnudelt und die Gesamtpunktverteilung erstellt.
 *
 * @author heike
 */
public class GesamtverteilungGenerator {

	private final List<Punktintervall> intervalle;

	private Map<Punktintervall, Punktintervallklasse> punktklassenMap = new HashMap<Punktintervall, Punktintervallklasse>();

	private final StatistikUtils statistikUtils;

	/**
	*
	*/
	public GesamtverteilungGenerator() {
		super();
		intervalle = Punktintervall.allMinikaenguruPunktintervalleDescending();
		statistikUtils = new StatistikUtils();
	}

	/**
	 * Erzeugt aus der gegebenen Liste von Loesungszetteln die Gesamtpunktverteilung. Die Überschrift und die
	 * Bewertungsgrundlage sind bereits gefüllt. Die Prozentränge sind aber noch nicht berechnet. Hierzu dann die
	 * Methoden von Gesamtpunktverteilung aufrufen.
	 *
	 * @param pLoesungszettel
	 * @param klasse
	 * @param jahr
	 * @return Gesamtpunktverteilung
	 */
	public Gesamtpunktverteilung create(List<Loesungszettel> pLoesungszettel, String jahr, String klasse) {
		initPunktklassen(pLoesungszettel);
		Gesamtpunktverteilung verteilung = createVerteilung(pLoesungszettel);
		initTexte(verteilung, jahr, klasse);
		return verteilung;
	}

	/**
	 * @param pLoesungszettel
	 */
	private void initPunktklassen(List<Loesungszettel> pLoesungszettel) {
		for (Loesungszettel zettel : pLoesungszettel) {
			Punktintervallklasse punktklasse = getPunktklasse(zettel);
			punktklasse.addLoesungszettel(zettel);
		}
	}

	/**
	 * @return
	 */
	private Gesamtpunktverteilung createVerteilung(List<Loesungszettel> pLoesungszettel) {
		Gesamtpunktverteilung verteilung = new Gesamtpunktverteilung();
		for (Punktintervall intervall : intervalle) {
			Punktintervallklasse klasse = punktklassenMap.get(intervall);
			if (klasse == null) {
				klasse = new Punktintervallklasse(intervall);
				punktklassenMap.put(intervall, klasse);
			}
			GesamtpunktverteilungItem item = new GesamtpunktverteilungItem();
			item.setAnzahl(klasse.getCardinality());
			item.setPunkte(intervall.toGesamtpunktVerteilungString());
			verteilung.addItem(item);
		}

		double median = statistikUtils.berechneMedian(pLoesungszettel);
		verteilung.setMedian("" + median);

		List<AufgabeErgebnis> ergebnisse = this.createAufgabenergebnisse(pLoesungszettel);
		verteilung.setEinzelergebnisse(ergebnisse);

		List<Rohpunktitem> rohpunkte = this.createRohpunkte(pLoesungszettel);
		verteilung.setRohpunktitems(rohpunkte);

		return verteilung;
	}

	/**
	 * Erzeugt aue den Lösungszetteln die Rohpunkitems mit ihren Prozenträngen
	 *
	 * @param loesungszettel
	 * @return
	 */
	private List<Rohpunktitem> createRohpunkte(List<Loesungszettel> loesungszettel) {
		Map<Double, Integer> anzahlNachPunkten = new HashMap<>();
		for (Loesungszettel einer : loesungszettel) {
			Double punkte = einer.berechneGesamtpunktzahl();
			Integer anzahl = anzahlNachPunkten.get(punkte);
			if (anzahl == null) {
				anzahl = 0;
			}
			anzahl++;
			anzahlNachPunkten.put(punkte, anzahl);

		}
		List<Rohpunktitem> result = new ArrayList<>();
		List<Double> keysSorted = new ArrayList<>();
		keysSorted.addAll(anzahlNachPunkten.keySet());
		Collections.sort(keysSorted);

		for (double key : keysSorted) {
			Integer anzahl = anzahlNachPunkten.get(key);
			result.add(new Rohpunktitem(key, anzahl));
		}

		return new StatistikUtils().berechneProzentraenge(result);
	}

	private List<AufgabeErgebnis> createAufgabenergebnisse(List<Loesungszettel> pLoesungszettel) {
		List<AufgabeErgebnis> ergebnisse = new ArrayList<AufgabeErgebnis>();
		for (int i = 0; i < 15; i++) {
			ergebnisse.add(statistikUtils.berechneAufgabeErgebnis(pLoesungszettel, i));
		}
		Collections.sort(ergebnisse);
		return ergebnisse;
	}

	/**
	 * @param verteilung
	 * @param klasse
	 * @param jahr
	 */
	private void initTexte(Gesamtpunktverteilung verteilung, String jahr, String klasse) {
		verteilung
			.setTitel(TexteProperties.getInstance().getPropertyFormated("headline", new Object[] { jahr, klasse }));
		verteilung.setBewertung(TexteProperties.getInstance().getProperty("bewertung.description"));
	}

	/**
	 * @param pZettel
	 * @return
	 */
	private Punktintervallklasse getPunktklasse(Loesungszettel pZettel) {
		Punktintervall intervall = findPunktintervall(pZettel);
		Punktintervallklasse klasse = punktklassenMap.get(intervall);
		if (klasse == null) {
			klasse = new Punktintervallklasse(intervall);
			punktklassenMap.put(intervall, klasse);
		}
		return klasse;
	}

	/**
	 * @param pZettel
	 * @return
	 */
	private Punktintervall findPunktintervall(Loesungszettel pZettel) {
		for (Punktintervall intervall : intervalle) {
			if (pZettel.gehoertZuPunktintervall(intervall)) {
				return intervall;
			}
		}
		throw new RuntimeException("Kein Punktintervall zu Loesungszettel mit Gesamtpunkten="
			+ pZettel.berechneGesamtpunktzahl() + " gefunden");
	}
}
