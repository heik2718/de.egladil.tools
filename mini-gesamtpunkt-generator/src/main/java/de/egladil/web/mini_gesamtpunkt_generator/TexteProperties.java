/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

/**
 * @author heike
 */
public class TexteProperties extends Properties {

  private static final String PROPRTIES_FILE_NAME = "/texte.properties";
  private static TexteProperties instance;
  /**
   *
   */
  private static final long serialVersionUID = 7607099060585307870L;

  /**
   *
   */
  private TexteProperties() {
	init();
  }

  /**
   *
   */
  private void init() {
	InputStream in = null;
	in = getClass().getResourceAsStream(PROPRTIES_FILE_NAME);
	try {
	  load(in);
	} catch (IOException e) {
	  System.err.println(e.getMessage());
	} finally {
	  IOUtils.closeQuietly(in);
	}
  }

  /**
   * @param pKey
   * @param pArgs
   * @return String
   */
  public String getPropertyFormated(String pKey, Object[] pArgs) {
	if (pKey == null) {
	  throw new IllegalArgumentException("pKey darf nicht null sein");
	}
	String val = getProperty(pKey);
	if (val != null) {
	  if (pArgs != null) {
		String result = MessageFormat.format(val, pArgs);
		return result;
	  }
	  return val;
	}
	throw new RuntimeException("keine property mit [key=" + pKey + "] definiert. Bitte " + PROPRTIES_FILE_NAME + " pruefen");
  }

  /**
   * @return the instance
   */
  public static final TexteProperties getInstance() {
	if (instance == null){
	  instance = new TexteProperties();
	}
    return instance;
  }
}
