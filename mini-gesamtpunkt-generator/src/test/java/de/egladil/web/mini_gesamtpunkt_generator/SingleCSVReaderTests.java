/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.web.mini_gesamtpunkt_generator.SingleCSVReader;

/**
 * @author heike
 */
public class SingleCSVReaderTests {

	private static final Logger LOG = LoggerFactory.getLogger(SingleCSVReaderTests.class);

	private SingleCSVReader reader;

	@Test
	public void testProcessStreamNull() {
		reader = new SingleCSVReader();
		InputStream in = null;
		try {
			reader.processResource(in);
			fail("keine IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			LOG.info(e.getMessage());
		}
	}

	@Test
	public void testProcessFileNull() {
		reader = new SingleCSVReader();
		File in = null;
		try {
			reader.processResource(in);
			fail("keine IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			LOG.info(e.getMessage());
		}
	}

	@Test
	public void testProcessInvalid() throws IOException {
		reader = new SingleCSVReader();
		try {
			File file = File.createTempFile("test", ".csv");
			file.delete();
			reader.processResource(file);
			fail("keine IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			LOG.info(e.getMessage());
		}
	}

	@Test
	public void testNormalFile() {
		reader = new SingleCSVReader();
		InputStream in = null;
		try {
			in = getClass().getResourceAsStream("/01.csv");
			List<String> result = reader.processResource(in);
			assertNotNull(result);
			assertEquals(28, result.size());
		} finally {
			IOUtils.closeQuietly(in);
		}
	}
}
