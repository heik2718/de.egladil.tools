/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import de.egladil.web.mcpunktgenerator_domain.IEinzelwertung;
import de.egladil.web.mcpunktgenerator_domain.Loesungszettel;
import de.egladil.web.mini_gesamtpunkt_generator.LoesungszettelGenerator;

/**
 * @author heike
 */
public class LoesungszettelGeneratorTest {

	private static final String PATH = System.getProperty("user.home") + File.separator + ".minikaengGenerator" + File.separator
		+ "developCSV";

	/**
	 *
	 */
	@Test
	public void testSetup(){
		System.out.println(PATH);
	}

	@Test
	public void testProcessDir() {
		LoesungszettelGenerator generator = new LoesungszettelGenerator();
		List<Loesungszettel> alle = generator.processDir(new File(PATH));
		assertNotNull(alle);
		int nummer = 1;
		for (Loesungszettel zettel : alle) {
			assertEquals(15, zettel.getEinzelwertungen().size());
			for (IEinzelwertung einzelwertung : zettel.getEinzelwertungen()) {
				assertNotNull(einzelwertung.getAufgabenkategorie());
				assertNotNull(einzelwertung.getWertung());
			}
			System.out.println(nummer + ": " + zettel.berechneGesamtpunktzahl());
			nummer++;
		}
	}
}
