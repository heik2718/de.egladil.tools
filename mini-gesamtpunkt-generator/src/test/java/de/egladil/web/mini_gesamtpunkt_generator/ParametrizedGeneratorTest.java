/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.egladil.web.mini_gesamtpunkt_generator.Generator;

/**
 * @author heike
 *
 */
@RunWith(Parameterized.class)
public class ParametrizedGeneratorTest {

	private String sourcePath;

	private String targetPath;

	private String jahr;

	private String klasse;

	@Parameters
	public static Collection<Object[]> data() {
		//@formatter:off
		Object[][] data = new Object[][] {
			{ "/home/heike/.minikaengGenerator/2015/csv-klasse1", "/home/heike/work/knobelarchiv_2/latex/minikaenguru/2015_minikaenguru_punktverteilung_klasse1.tex", "2014",  "1" }
		   ,{ "/home/heike/.minikaengGenerator/2015/csv-klasse2", "/home/heike/work/knobelarchiv_2/latex/minikaenguru/2015_minikaenguru_punktverteilung_klasse2.tex", "2014",  "2" }
		   ,{ "/home/heike/.minikaengGenerator/2014/csv-klasse1", "/home/heike/work/knobelarchiv_2/latex/minikaenguru/2014_minikaenguru_punktverteilung_klasse1.tex", "2014",  "1" }
		   ,{ "/home/heike/.minikaengGenerator/2014/csv-klasse2", "/home/heike/work/knobelarchiv_2/latex/minikaenguru/2014_minikaenguru_punktverteilung_klasse2.tex", "2014",  "2" }
		   ,{ "/home/heike/.minikaengGenerator/2013/csv", "/home/heike/work/knobelarchiv_2/latex/minikaenguru/2013_minikaenguru_punktverteilung_klasse2.tex", "2013",  "2" }
		   ,{ "/home/heike/.minikaengGenerator/2012/csv", "/home/heike/work/knobelarchiv_2/latex/minikaenguru/2012_minikaenguru_punktverteilung_klasse2.tex", "2012",  "2" }
		   ,{ "/home/heike/.minikaengGenerator/2011/csv", "/home/heike/work/knobelarchiv_2/latex/minikaenguru/2011_minikaenguru_punktverteilung_klasse2.tex", "2011",  "2" }
		   ,{ "/home/heike/.minikaengGenerator/2010/csv", "/home/heike/work/knobelarchiv_2/latex/minikaenguru/2010_minikaenguru_punktverteilung_klasse2.tex", "2010",  "2" }
		   };
		//@formatter:on
		return Arrays.asList(data);
	}

	/**
	 * @param sourcePath
	 * @param targetPath
	 * @param jahr
	 * @param klasse
	 */
	public ParametrizedGeneratorTest(String sourcePath, String targetPath, String jahr, String klasse) {
		this.sourcePath = sourcePath;
		this.targetPath = targetPath;
		this.jahr = jahr;
		this.klasse = klasse;
	}

	@Test
	public void testRun() {
		Generator generator = new Generator(this.targetPath);
		generator.run(sourcePath, jahr, klasse);
	}
}
