/**
 *
 */
package de.egladil.web.mini_gesamtpunkt_generator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import de.egladil.web.mcpunktgenerator_domain.AufgabeErgebnis;
import de.egladil.web.mcpunktgenerator_domain.Loesungszettel;
import de.egladil.web.mcpunktgenerator_domain.Wertung;

/**
 * @author heike
 *
 */
public class StatistikUtilsTest {

	@Test
	public final void sollArithmetischesMittelAllerPunkteBerechnen() {
		List<Loesungszettel> loesungszettel = new LoesungszettelGenerator()
			.processDir(new File("/home/heike/.minikaengGenerator/2015/csv-klasse2"));

		StatistikUtils utils = new StatistikUtils();

		double mittelwert = utils.berechneArithmetischesMittel(loesungszettel);

		assertTrue(mittelwert > 0);
		System.out.println("Mittelwert = " + mittelwert);
	}

	@Test
	public final void sollMedianAllerPunkteBerechnen() {
		List<Loesungszettel> loesungszettel = new LoesungszettelGenerator()
			.processDir(new File("/home/heike/.minikaengGenerator/2015/csv-klasse1"));

		StatistikUtils utils = new StatistikUtils();

		double mittelwert = utils.berechneMedian(loesungszettel);

		assertTrue(mittelwert > 0);
		System.out.println("Median = " + mittelwert);
	}

	/**
	 *
	 */
	@Test
	public void sollAufgabenergebnisFuerEinenIndexBerechnen() {
		List<Loesungszettel> loesungszettel = new LoesungszettelGenerator()
			.processDir(new File("/home/heike/.minikaengGenerator/2015/csv-klasse2"));

		int index = 4;

		AufgabeErgebnis aufgabeErgebnis = new StatistikUtils().berechneAufgabeErgebnis(loesungszettel, index);

		assertNotNull(aufgabeErgebnis);
		System.out.println(aufgabeErgebnis.toString());
	}

	@Test
	public void soll_prozentrang_fuer_beste_ergebnisse_berechnen() throws Exception {
		List<Loesungszettel> loesungszettel = new LoesungszettelGenerator()
			.processDir(new File("/home/heike/.minikaengGenerator/developEinFile"));

		StatistikUtils utils = new StatistikUtils();
		List<Double> rohpunkte = utils.getMesswerte(loesungszettel);
		Collections.sort(rohpunkte);
		for (Double d : rohpunkte) {
			System.out.println(d);
		}
	}

	@Test
	public void soll_moegliche_punkte_berechnen() {
		List<Loesungszettel> alleMoeglichen = this.getLoesungszettelMitAllenMoeglichenPunkten();

		Map<Double, List<Loesungszettel>> loesungszettelNachPunkten = new HashMap<>();
		// List<Double> alleMoeglichenPunkte = new ArrayList<>();
		for (Loesungszettel l : alleMoeglichen) {
			Double punkte = l.berechneGesamtpunktzahl();
			List<Loesungszettel> z = loesungszettelNachPunkten.get(punkte);
			if (z == null) {
				z = new ArrayList<>();
				loesungszettelNachPunkten.put(punkte, z);
			}
			if (!z.contains(l)) {
				z.add(l);
			}

			// if (!alleMoeglichenPunkte.contains(punkte)) {
			// alleMoeglichenPunkte.add(punkte);
			// }
		}
		System.out.println("Anzahl verschiedener Lösungszettel: " + alleMoeglichen.size());
		System.out.println("Anzahl verschiedener Punkte: " + loesungszettelNachPunkten.size());

		Set<Double> keys = loesungszettelNachPunkten.keySet();
		List<Double> punkte = new ArrayList<>();
		punkte.addAll(keys);
		Collections.sort(punkte);

		for (Double p : punkte) {
			List<Loesungszettel> z = loesungszettelNachPunkten.get(p);
//			System.out.println(p);
			System.out.println(p + " - Anzahl : " + z.size());
			// for (Loesungszettel l : z) {
			// // System.out.println(l.toString());
			//// System.out.println(punkte + ": " + z.size());
			// }
			int i = 0;
		}

		// System.out.println("Anzahl verschiedener Punkte: " + alleMoeglichenPunkte.size());
		// Collections.sort(alleMoeglichenPunkte);
		// for (Double d : alleMoeglichenPunkte) {
		// System.out.println(d);
		// }
	}

	private List<Loesungszettel> getLoesungszettelMitAllenMoeglichenPunkten() {
		List<Loesungszettel> result = new ArrayList<>();
		List<Wertung[]> fuenfer = getVerschiedeneFuenfergruppen();
		String[] aufgaben3 = new String[] { "0", "1", "2", "3", "4" };
		String[] aufgaben4 = new String[] { "5", "6", "7", "8", "9" };
		String[] aufgaben5 = new String[] { "10", "11", "12", "13", "14" };

		for (int i = 0; i < fuenfer.size(); i++) {
			for (int j = 0; j < fuenfer.size(); j++) {
				for (int k = 0; k < fuenfer.size(); k++) {
					Wertung[] drei = fuenfer.get(i);
					Wertung[] vier = fuenfer.get(j);
					Wertung[] fuenf = fuenfer.get(k);

					Loesungszettel zettel = Loesungszettel.create(aufgaben3, drei, aufgaben4, vier, aufgaben5, fuenf);
					// System.out.println("Punkte: " + zettel.berechneGesamtpunktzahl());
					result.add(zettel);

				}
			}
		}

		return result;
	}

	private List<Wertung[]> getVerschiedeneFuenfergruppen() {
		List<Wertung[]> fuenfer = new ArrayList<>();
		// 5 falsch
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.FALSCH, Wertung.FALSCH,
				Wertung.FALSCH };
			fuenfer.add(wertungen);
		}

		// 4 falsch
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.FALSCH, Wertung.FALSCH,
				Wertung.RICHTIG };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.FALSCH, Wertung.FALSCH,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}

		// 3 falsch
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.FALSCH, Wertung.RICHTIG,
				Wertung.RICHTIG };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.FALSCH, Wertung.RICHTIG,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.FALSCH, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}

		// 2 falsch
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.RICHTIG, Wertung.RICHTIG,
				Wertung.RICHTIG };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.RICHTIG, Wertung.RICHTIG,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.RICHTIG, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.FALSCH, Wertung.LEER, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}

		// 1 falsch
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.RICHTIG, Wertung.RICHTIG, Wertung.RICHTIG,
				Wertung.RICHTIG };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.RICHTIG, Wertung.RICHTIG, Wertung.RICHTIG,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.RICHTIG, Wertung.RICHTIG, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.RICHTIG, Wertung.LEER, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.FALSCH, Wertung.LEER, Wertung.LEER, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}

		// 0 falsch
		{
			Wertung[] wertungen = new Wertung[] { Wertung.RICHTIG, Wertung.RICHTIG, Wertung.RICHTIG, Wertung.RICHTIG,
				Wertung.RICHTIG };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.RICHTIG, Wertung.RICHTIG, Wertung.RICHTIG, Wertung.RICHTIG,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.RICHTIG, Wertung.RICHTIG, Wertung.RICHTIG, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.RICHTIG, Wertung.RICHTIG, Wertung.LEER, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.RICHTIG, Wertung.LEER, Wertung.LEER, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		{
			Wertung[] wertungen = new Wertung[] { Wertung.LEER, Wertung.LEER, Wertung.LEER, Wertung.LEER,
				Wertung.LEER };
			fuenfer.add(wertungen);
		}
		return fuenfer;
	}
}
