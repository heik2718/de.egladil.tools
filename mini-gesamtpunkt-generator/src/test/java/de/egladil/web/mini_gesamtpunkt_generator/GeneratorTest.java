//=====================================================
// Projekt: mini-gesamtpunkt-generator
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.web.mini_gesamtpunkt_generator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.junit.Before;
import org.junit.Test;

import de.egladil.web.mcpunktgenerator_domain.Gesamtpunktverteilung;

/**
 * GesamtpunktverteilungGeneratorTest
 */
public class GeneratorTest {

	private JAXBContext jaxbContext;

	@Before
	public void setUp() throws Exception {
		jaxbContext = JAXBContext.newInstance(Gesamtpunktverteilung.class);
	}

	@Test
	public void soll_vollstaendige_gesamtpunktverteilung_erzeugen() throws Exception {
		// Arrange
		final String targetPath = "/home/heike/work/knobelarchiv_2/latex/minikaenguru/ttt.tex";
		final Generator generator = new Generator(targetPath);

		// Act
		final Gesamtpunktverteilung gesamtpunktverteilung = generator
			.createVerteilung("/home/heike/.minikaengGenerator/developEinFile", "2000", "2");

		// Assert
		assertNotNull(gesamtpunktverteilung);
		assertNotNull(gesamtpunktverteilung.getItemsSorted());
		assertNotNull(gesamtpunktverteilung.getEinzelergebnisse());
		assertNotNull(gesamtpunktverteilung.getRohpunktitems());

		assertTrue(gesamtpunktverteilung.isKannGeneriertWerden());

		final StringWriter writer = new StringWriter();
		jaxbContext.createMarshaller().marshal(gesamtpunktverteilung, writer);
		System.out.println(writer.toString());

	}

	@Test
	public void createOutputKlappt() throws Exception {
		// Arrange
		try (InputStream in = getClass().getResourceAsStream("/mini2019/klasse2.xml")) {
			final JAXBContext jaxbContext = JAXBContext.newInstance(Gesamtpunktverteilung.class);
			final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//			unmarshaller.setListener(new GesamtpunktverteilungUnmarshallerListener());
			final Gesamtpunktverteilung verteilung = (Gesamtpunktverteilung) unmarshaller.unmarshal(in);
			// verteilung.berechneProzentraenge();
			verteilung.setKannGeneriertWerden(true);
			final Generator generator = new Generator("/media/veracrypt1/ag_arbeit/minikaenguru/_punktverteilungen/2019/2019_klasse2.tex");
			generator.createOutput(verteilung);
		}
	}
}
