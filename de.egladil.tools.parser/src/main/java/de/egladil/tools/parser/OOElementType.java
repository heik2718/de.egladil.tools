//=====================================================
// Projekt: de.egladil.tools.parser
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.parser;

/**
 * OOElementType
 */
public enum OOElementType {

	CELL,
	ROW;

}
