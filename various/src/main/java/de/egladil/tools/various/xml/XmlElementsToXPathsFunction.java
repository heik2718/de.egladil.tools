//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.tools.various.StringInputStreamCache;

public class XmlElementsToXPathsFunction implements Function<String, List<String>> {

	private static final Logger LOG = LoggerFactory.getLogger(XmlElementsToXPathsFunction.class);

	private boolean fromRoot = false;

	/**
	 * Das Resultat wird in umgekehrter Reihenfolge ausgegeben.
	 *
	 * @return
	 */
	public static XmlElementsToXPathsFunction createWithReversedOrder() {
		XmlElementsToXPathsFunction result = new XmlElementsToXPathsFunction();
		result.fromRoot = true;
		return result;
	}

	@Override
	public List<String> apply(String xml) {

		try (StringInputStreamCache cache = new StringInputStreamCache()) {

			cache.init(xml);

			Map<Integer, String> levelTagMap = new HashMap<>();
			int actualIndex = 0;
			List<String> result = new ArrayList<>();

			XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
			XMLStreamReader parser = xmlInputFactory.createXMLStreamReader(cache.getTextAsInputStream());

			while (parser.hasNext()) {
				parser.next();

				String localName = null;
				String actualElementName = null;

				switch (parser.getEventType()) {

				case XMLStreamConstants.START_ELEMENT:
					localName = parser.getLocalName();
					if (!localName.equals(actualElementName)) {
						actualElementName = localName;
					}
					levelTagMap.put(Integer.valueOf(actualIndex++), localName);

					break;
				case XMLStreamConstants.END_ELEMENT:
					localName = parser.getLocalName();
					actualIndex--;

					StringBuffer sb = new StringBuffer();
					for (int i = 0; i < actualIndex; i++) {
						sb.append(levelTagMap.get(Integer.valueOf(i)));
						sb.append("/");
					}

					sb.append(localName);
					String path = sb.toString();
					if (!result.contains(path)) {
						result.add(path);
					}
					break;
				default:
					break;
				}
			}

			if (fromRoot) {
				return result;
			}
			return revert(result);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return new ArrayList<>();
		}
	}

	private List<String> revert(List<String> list) {
		List<String> result = new ArrayList<>();
		for (int i = list.size() - 1; i >= 0; i--) {
			result.add(list.get(i));
		}
		return result;
	}
}
