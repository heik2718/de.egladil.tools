//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================
package de.egladil.tools.various.xml;

import java.util.List;
import java.util.function.Function;

import org.w3c.dom.Document;

/**
 * @author winkelv
 *
 */
public class DocumentToElementXPathsFunction implements Function<Document, List<String>> {

	private XmlElementsToXPathsFunction wrappedFunction = new XmlElementsToXPathsFunction();

	/**
	 *
	 */
	public static DocumentToElementXPathsFunction createWithDescendingOrder() {
		DocumentToElementXPathsFunction result = new DocumentToElementXPathsFunction();
		result.wrappedFunction = XmlElementsToXPathsFunction.createWithReversedOrder();
		return result;
	}

	@Override
	public List<String> apply(Document document) {
		return wrappedFunction.apply(new SerializeDomFunction().apply(document));
	}

}
