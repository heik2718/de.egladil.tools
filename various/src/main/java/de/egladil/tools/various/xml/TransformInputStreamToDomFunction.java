//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import java.io.InputStream;
import java.util.function.Function;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

/**
 * @author TransformInputStreamToDomFunction
 *
 */
public class TransformInputStreamToDomFunction implements Function<InputStream, Document> {

	@Override
	public Document apply(InputStream in) {
		if (in == null) {
			throw new IllegalArgumentException("in null");
		}
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(in);
			return doc;
		} catch (Exception e) {
			throw new RuntimeException("Fehler beim Generieren des w3c-Documents " + e.getMessage(), e);
		}
	}
}
