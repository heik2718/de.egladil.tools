//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import java.util.function.Function;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/**
 * CopyDomFunction kopiert ein Document
 */
public class CopyDomFunction implements Function<Document, Document> {

	private static final Logger LOG = LoggerFactory.getLogger(CopyDomFunction.class);

	@Override
	public Document apply(Document document) {

		try {
			TransformerFactory tfactory = TransformerFactory.newInstance();
			Transformer tx = tfactory.newTransformer();
			DOMSource source = new DOMSource(document);
			DOMResult result = new DOMResult();
			tx.transform(source, result);
			return (Document) result.getNode();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new RuntimeException("Exception beim Kopieren eines Documents", e);
		}
	}

}
