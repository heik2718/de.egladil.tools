//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import java.util.function.Function;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author RemoveLeafAllElementsFunction
 */
public class RemoveLeafAllElementsFunction implements Function<Document, Document> {

	private static final Logger LOG = LoggerFactory.getLogger(RemoveLeafAllElementsFunction.class);

	@Override
	public Document apply(Document sourceDocument) {

		Document kopie = new CopyDomFunction().apply(sourceDocument);

		try {
			XPathExpression xpath = XPathFactory.newInstance().newXPath().compile("//*[count(./*) = 0]");

			NodeList nodeList = (NodeList) xpath.evaluate(kopie, XPathConstants.NODESET);

			for (int i = 0; i < nodeList.getLength(); i++) {
				final Element el = (Element) nodeList.item(i);
				el.getParentNode().removeChild(el);
			}

			return kopie;

		} catch (XPathExpressionException e) {
			LOG.error(e.getMessage(), e);
			throw new RuntimeException("Fehler beim Leeren eines Document");

		}

	}

}
