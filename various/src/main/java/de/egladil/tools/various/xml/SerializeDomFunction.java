//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import java.io.IOException;
import java.io.StringWriter;
import java.util.function.Function;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

/**
 * @author SerializeDomFunction
 *
 */
public class SerializeDomFunction implements Function<Document, String> {

	@Override
	public String apply(Document document) {

		try (StringWriter sw = new StringWriter()) {
			TransformerFactory tf = TransformerFactory.newInstance();

			Transformer t = tf.newTransformer();

			t.transform(new DOMSource(document), new StreamResult(sw));
			return sw.toString();
		} catch (TransformerException | IOException e) {
			throw new RuntimeException("Unerwarteter Fehler beim Serialisieren: " + e.getMessage());
		}
	}
}
