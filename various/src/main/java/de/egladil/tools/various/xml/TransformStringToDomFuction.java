//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import java.util.function.Function;

import org.w3c.dom.Document;

import de.egladil.tools.various.StringInputStreamCache;

/**
 * @author TransformStringToDomFuction
 *
 */
public class TransformStringToDomFuction implements Function<String, Document> {

	@Override
	public Document apply(String xml) {
		if (xml == null) {
			throw new IllegalArgumentException("xml null");
		}
		try (StringInputStreamCache cache = new StringInputStreamCache()) {
			cache.init(xml);
			return new TransformInputStreamToDomFunction().apply(cache.getTextAsInputStream());
		} catch (Exception e) {
			throw new RuntimeException("Fehler beim parsen des xml: " + e.getMessage(), e);
		}
	}

}
