//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================
package de.egladil.tools.various.xml;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author RemoveEmptyNodesFunction bekommt ein teilweise gefülltes Document und
 *         entfernt daraus alle leeren Nodes.
 *
 */
public class RemoveEmptyNodesFunction implements Function<Document, Document> {

	private static final Logger LOG = LoggerFactory.getLogger(RemoveEmptyNodesFunction.class);

	@Override
	public Document apply(Document sourceDocument) {

		Document kopie = new CopyDomFunction().apply(sourceDocument);

		Element rootElement = kopie.getDocumentElement();
		traverse(rootElement.getChildNodes());
		return null;
	}

	private void traverse(NodeList nodeList) {

		int length = nodeList.getLength();
		for (int i = 0; i < length; i++) {
			Node child = nodeList.item(i);

			if (child.hasChildNodes()) {
				traverse(child.getChildNodes());
			}

			switch (child.getNodeType()) {
			case Node.ELEMENT_NODE:

				String nodeName = child.getNodeName();
				if ("F00000045".equals(nodeName)) {
					System.out.println("Vorname");
				}

				System.out.println("Element: " + child.getNodeName());
				break;
			case Node.TEXT_NODE:
				System.out.println("Text: " + child.getNodeValue());
				break;
			default:
				break;
			}
		}
	}
}
