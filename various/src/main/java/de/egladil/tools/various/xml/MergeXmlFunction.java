//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * MergeXmlFunction verwendet ein XML, das alle Tags enthält, aber vollständig
 * leer ist (completeXml) und ein XML gleicher Strukur, das nur die gefüllten
 * Tags enthält (reduced), um ein gemergedes XML zu erzeugen.
 *
 */
public class MergeXmlFunction implements BiFunction<String, String, Optional<String>> {

	private static final Logger LOG = LoggerFactory.getLogger(MergeXmlFunction.class);

	private Document reducedDom;

	private Document mergedDom;

	private List<String> xpathsOfLeaveNodes;

	@Override
	public Optional<String> apply(String completeEmptyXml, String reducedXml) {

		initXpathsOfLeaveNodes(completeEmptyXml);

		if (xpathsOfLeaveNodes.isEmpty()) {
			return Optional.empty();
		}

		Document completeDom = new TransformStringToDomFuction().apply(completeEmptyXml);

		Document mergedDom = new CopyDomFunction().apply(completeDom);

		Element mergedRoot = reducedDom.getDocumentElement();

		for (String xpath : xpathsOfLeaveNodes) {
//			mergedRoot.
		}

		return Optional.empty();
	}

	private void initXpathsOfLeaveNodes(String xml) {

		List<String> allXpaths = new XmlElementsToXPathsFunction().apply(xml);

		int maxLevel = allXpaths.stream().map(s -> Integer.valueOf(s.split("/").length)).collect(Collectors.toList())
				.stream().reduce(0, (max, i) -> {
					max = max < i ? i : max;
					return max;
				});

		xpathsOfLeaveNodes = allXpaths.stream().filter(s -> s.split("/").length - 1 == maxLevel)
				.collect(Collectors.toList());
	}
}
