//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;

/**
 * StringInputStreamCache dient zum Zwischenspeichern von Daten aus einem
 * InputStream, wenn diese mehrfach benötigt werden.
 */
public class StringInputStreamCache implements AutoCloseable {

	private String text;

	/**
	 *
	 * @param text String
	 * @return StringInputStreamCache
	 */
	public StringInputStreamCache init(final String text) {
		if (text == null) {
			throw new IllegalArgumentException("text null");
		}
		this.text = text;
		return this;
	}

	/**
	 * Legt die (String)- Daten des InputStreams in eigener Variable ab.
	 *
	 * @param in InputStream darf nicht null sein.
	 * @return StringInputStreamCache (fluent API)
	 */
	public StringInputStreamCache init(final InputStream in) {
		if (in == null) {
			throw new IllegalArgumentException("in null");
		}
		try (StringWriter sw = new StringWriter()) {
			IOUtils.copy(in, sw, "UTF-8");
			text = sw.toString();
			return this;
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 *
	 * @return String
	 */
	public String getText() {
		if (text == null) {
			throw new IllegalStateException("Falsche Reihenfolge: erst init() aufrufen");
		}
		return text;
	}

	/**
	 * Gibt den gecacheten text als InputStream zurück.
	 *
	 * @return InputStream
	 */
	public InputStream getTextAsInputStream() {
		if (text == null) {
			throw new IllegalStateException("Falsche Reihenfolge: erst init() aufrufen");
		}
		InputStream targetStream = new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8));
		return targetStream;
	}

	@Override
	public void close() throws Exception {
		text = null;
	}
}
