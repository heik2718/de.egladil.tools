//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;

/**
 * TransformToDOMCommandTest
 */

public class TransformToDOMTest {

	private static final String PATH_XML = "/xml/wohngeld_xfall_fachdaten_default.xml";

	private static final String PATH_EEB_INVALID = "/xml/keinXml.txt";

	@Test
	void getDom_throws_IllegalArgumentException_1() {

		try {
			new TransformInputStreamToDomFunction().apply(null);
			fail("keine IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			assertEquals("in null", e.getMessage());
		}
	}

	@Test
	void getDom_throws_IllegalArgumentException_2() {
		try {
			new TransformStringToDomFuction().apply(null);
			fail("keine IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			assertEquals("xml null", e.getMessage());
		}

	}

	@Test
	void getDom_klappt() throws Exception {
		try (InputStream in = getClass().getResourceAsStream(PATH_XML)) {
			Document doc = new TransformInputStreamToDomFunction().apply(in);
			assertNotNull(doc);
		}
	}

	@Test
	void getDom_kein_xml() throws Exception {
		try (InputStream in = getClass().getResourceAsStream(PATH_EEB_INVALID)) {
			new TransformInputStreamToDomFunction().apply(in);

			fail("keine RuntimeException");
		} catch (RuntimeException e) {
			assertEquals("Fehler beim Generieren des w3c-Documents Content ist nicht zulässig in Prolog.",
					e.getMessage());
		}
	}
}
