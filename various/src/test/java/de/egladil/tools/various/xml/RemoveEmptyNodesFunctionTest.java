//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================
package de.egladil.tools.various.xml;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;

/**
 * @author RemoveEmptyNodesFunctionTest
 *
 */
public class RemoveEmptyNodesFunctionTest {

	@Test
	void schrumpfeDomKlappt() throws Exception {

		// Arrange
		String xml = VariousTestUtils.readFromClasspath("/xml/wohngeld_xfall_fachdaten_ausgefuellt.xml");
//		System.out.println(xml);
		Document source = new TransformStringToDomFuction()
				.apply(xml);


		String path = "fim.S00000116.00000116001001/G00000448/G00000188/F00000045";


		XPathExpression expr = XPathFactory.newInstance().newXPath().compile(path);
		Object obj = expr.evaluate(source, XPathConstants.NODESET);

		// Act
		Document result = new RemoveEmptyNodesFunction().apply(source);

	}

}
