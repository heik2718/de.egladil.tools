//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;

/**
 * @author VariousTestUtils
 *
 */
public class VariousTestUtils {

	/**
	 *
	 */
	private VariousTestUtils() {
	}

	public static String readFromClasspath(String path) throws IOException {

		try (InputStream in = VariousTestUtils.class.getResourceAsStream(path); StringWriter sw = new StringWriter()) {

			IOUtils.copy(in, sw, "UTF-8");

			return sw.toString();
		}

	}

}
