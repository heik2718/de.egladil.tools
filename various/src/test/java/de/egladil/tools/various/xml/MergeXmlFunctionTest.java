//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * MergeXmlFunctionTest
 */
public class MergeXmlFunctionTest {

	private static final String PATH_COMPLETE_XML = "/xml/wohngeld_xfall_fachdaten_default.xml";

	private static final String PATH_EXPORTED_XML = "/xml/wohngeld_xfall_exported.xml";

	private static final String PATH_EXPECTED_XML = "/xml/wohngeld_xfall_fachdaten_merged.xml";

	private String completeXml;

	private String exportedXml;

	private String expectedXml;

	@BeforeEach
	public void setUp() throws IOException {
		completeXml = VariousTestUtils.readFromClasspath(PATH_COMPLETE_XML);
		exportedXml = VariousTestUtils.readFromClasspath(PATH_EXPORTED_XML);
		expectedXml = VariousTestUtils.readFromClasspath(PATH_EXPECTED_XML);
	}

	@Test
	void testApplySuccess() {

		Optional<String> optMerged = new MergeXmlFunction().apply(completeXml, exportedXml);

		assertTrue(optMerged.isPresent());

		assertEquals(expectedXml, optMerged.get());

	}
}
