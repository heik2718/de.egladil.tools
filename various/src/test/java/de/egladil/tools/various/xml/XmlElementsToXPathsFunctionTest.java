//=====================================================
//Projekt: various
//(c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;

/**
 * XmlElementsToXPathsFunctionTest
 */
public class XmlElementsToXPathsFunctionTest {

	@Test
	void testApplyToExportedSuccess() throws Exception {
		// Arrange
		String exportedXml = VariousTestUtils.readFromClasspath("/xml/wohngeld_xfall_exported.xml");

		// @formatter:off
		List<String> expectedResult = Arrays
				.asList(new String[] { "fim.S00000116.00000116001001/G00000448/G00000188/F00000698",
						"fim.S00000116.00000116001001/G00000448/G00000188/F00000045",
						"fim.S00000116.00000116001001/G00000448/G00000188/F00000066",
						"fim.S00000116.00000116001001/G00000448/G00000188/F00000067",
						"fim.S00000116.00000116001001/G00000448/G00000188/F00000065",
						"fim.S00000116.00000116001001/G00000448/G00000188/G00000368/F00000039",
						"fim.S00000116.00000116001001/G00000448/G00000188/G00000368",
						"fim.S00000116.00000116001001/G00000448/G00000188",
						"fim.S00000116.00000116001001/G00000448",
						"fim.S00000116.00000116001001" });
		// @formatter:on

		List<String> xpathList = XmlElementsToXPathsFunction.createWithReversedOrder().apply(exportedXml);

		// xpathList.stream().forEach(System.out::println);

		assertEquals(expectedResult.size(), xpathList.size());

		for (int i = 0; i < xpathList.size(); i++) {
			System.out.println(xpathList.get(i));
			assertEquals(expectedResult.get(i), xpathList.get(i));
		}

	}

	@Test
	void testApplyToEmpty() throws Exception {
		// Arrange
		String exportedXml = VariousTestUtils.readFromClasspath("/xml/wohngeld_xfall_fachdaten_default.xml");

		List<String> xpathList = new XmlElementsToXPathsFunction().apply(exportedXml);

		assertEquals(238, xpathList.size());

		xpathList.stream().forEach(System.out::println);

		int maxLevel = xpathList.stream().map(s -> Integer.valueOf(s.split("/").length)).collect(Collectors.toList())
				.stream().reduce(0, (max, i) -> {
					max = max < i ? i : max;
					return max;
				});

		List<String> xpathsOfLeaveNodes = xpathList.stream().filter(s -> s.split("/").length - 1 == maxLevel)
				.collect(Collectors.toList());

		xpathsOfLeaveNodes.stream().forEach(System.out::println);

	}

	@Test
	void testErzeugeTeildokumentMitGefuelltenFeldern() throws Exception {
		// Arrange
		String exportedXml = VariousTestUtils.readFromClasspath("/xml/wohngeld_xfall_fachdaten_ausgefuellt.xml");

		Document dom = new TransformStringToDomFuction().apply(exportedXml);

		List<String> xpathList = new XmlElementsToXPathsFunction().apply(exportedXml);

		assertEquals(238, xpathList.size());

		xpathList.stream().forEach(System.out::println);
	}
}
