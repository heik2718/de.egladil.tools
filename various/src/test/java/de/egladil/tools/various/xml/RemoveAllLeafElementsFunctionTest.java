//=====================================================
// Projekt: various
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.tools.various.xml;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;

/**
 * @author RemoveAllLeafElementsFunctionTest
 *
 */
public class RemoveAllLeafElementsFunctionTest {


	@Test
	void removeFromGeneratedFim() throws IOException {

		// Arrange
		String pathSource = "/xml/do-not-change-formatting/wohngeld_xfall_fachdaten_generiert.xml";
		String pathExpected = "/xml/do-not-change-formatting/result_elements_removed.xml";

		String expected = VariousTestUtils.readFromClasspath(pathExpected);

		Document source = new TransformStringToDomFuction().apply(VariousTestUtils.readFromClasspath(pathSource));

		// Act
		Document result = new RemoveLeafAllElementsFunction().apply(source);

		// Assert
		String actual = new SerializeDomFunction().apply(result);
		assertEquals(expected, actual);
	}

}
